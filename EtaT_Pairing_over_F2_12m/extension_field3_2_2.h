#pragma once

/*

extension field: $\mathbb F_{2^{3m}}$の定義
\mathbb F_{2^{3m}} = \mathbb F_2[x]/(x^3+x+1)

*/
template<class T> class F2_ExtField3 {

public:

	static const int ext_degree = 3;
	static const int base_degree = T::ext_degree;
	static const int regLength = T::regLength;

	T xBF[3];



	F2_ExtField3() {}

#if 0
	F2_ExtField3(const T& a0, const T& a1, const T& a2)
		: xBF[0](a0)
		, xBF[1](a1)
		, xBF[2](a2)
	{}

	F2_ExtField3(const T *a)
		: xBF[0](a[0])
		, xBF[1](a[1])
		, xBF[2](a[2])
	{}
#endif

	void clear() {

		xBF[0].clear();
		xBF[1].clear();
		xBF[2].clear();
	}

	void clearUpDeg() {

		xBF[0].clearUpDeg();
		xBF[1].clearUpDeg();
		xBF[2].clearUpDeg();
	}

	void setRandom() {

		xBF[0].setRandom();
		xBF[1].setRandom();
		xBF[2].setRandom();
	}

	void copyReg(const F2_ExtField3& a) {

		this->xBF[0].copyReg(a.xBF[0]);
		this->xBF[1].copyReg(a.xBF[1]);
		this->xBF[2].copyReg(a.xBF[2]);
	}

	void plusOne() {

		xBF[0].plusOne();
	}

	friend F2_ExtField3 operator^(const F2_ExtField3& a, const F2_ExtField3& b) {

		F2_ExtField3 c;

		c.xBF[0] = a.xBF[0] ^ b.xBF[0];
		c.xBF[1] = a.xBF[1] ^ b.xBF[1];
		c.xBF[2] = a.xBF[2] ^ b.xBF[2];
		return c;
	}

	friend F2_ExtField3 operator+(const F2_ExtField3& a, const F2_ExtField3& b) {

		F2_ExtField3 c;

		c.xBF[0] = a.xBF[0] ^ b.xBF[0];
		c.xBF[1] = a.xBF[1] ^ b.xBF[1];
		c.xBF[2] = a.xBF[2] ^ b.xBF[2];
		return c;
	}

	friend F2_ExtField3 operator-(const F2_ExtField3& a, const F2_ExtField3& b) {

		F2_ExtField3 c;

		c.xBF[0] = a.xBF[0] ^ b.xBF[0];
		c.xBF[1] = a.xBF[1] ^ b.xBF[1];
		c.xBF[2] = a.xBF[2] ^ b.xBF[2];
		return c;
	}

	friend bool operator==(const F2_ExtField3& a, const F2_ExtField3& b) {

		bool result = true;

		result &= (a.xBF[0] == b.xBF[0]);
		result &= (a.xBF[1] == b.xBF[1]);
		result &= (a.xBF[2] == b.xBF[2]);
		return result;
	}

	F2_ExtField3 operator^=(const F2_ExtField3& b) {

		xBF[0] ^= b.xBF[0];
		xBF[1] ^= b.xBF[1];
		xBF[2] ^= b.xBF[2];
		return *this;
	}

	F2_ExtField3 operator+=(const F2_ExtField3& b) {

		xBF[0] ^= b.xBF[0];
		xBF[1] ^= b.xBF[1];
		xBF[2] ^= b.xBF[2];
		return *this;
	}

	F2_ExtField3 operator-=(const F2_ExtField3& b) {

		xBF[0] ^= b.xBF[0];
		xBF[1] ^= b.xBF[1];
		xBF[2] ^= b.xBF[2];
		return *this;
	}

	/*

	reduction with defining polynomial x^3 + x + 1

	a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0
	= (a_2 + a_4)w^2 + (a_1 + a_3 + a_4)w + a_0 + a_3

	*/
	void reduction(const F2_ExtField_Double<F2_ExtField3>& c) {

		T tmp;

		// BF[2] = a2 + a4
		this->xBF[2] = c.low.xBF[2] + c.high.xBF[1];

		// BF[1] = a1 + a3 + a4
		this->xBF[1] = c.low.xBF[1] + c.high.xBF[1];
		this->xBF[1] += c.high.xBF[0];

		// BF[0] = a0 + a3
		this->xBF[0] = c.low.xBF[0] + c.high.xBF[0];
	}

	// Karatsuba 3
	static void mulShAdd(F2_ExtField3& c, const F2_ExtField3& a, const F2_ExtField3& b) {

		T v1, vST[3];
		F2_ExtField_Double<F2_ExtField3> result;

		// res[0] = a[0]b[0]
		T::mulShAdd(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[4] = a[2]b[2]
		T::mulShAdd(result.high.xBF[1], a.xBF[2], b.xBF[2]);

		// v1 = a[1]b[1]
		T::mulShAdd(v1, a.xBF[1], b.xBF[1]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulShAdd(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulShAdd(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulShAdd(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v1;

		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= result.high.xBF[1];
		result.low.xBF[2] += v1;

		result.high.xBF[0] = vST[2] - v1;
		result.high.xBF[0] -= result.high.xBF[1];

		c.reduction(result);
	}

	static void mulCombR2L_RegSize(F2_ExtField3& c, const F2_ExtField3& a, const F2_ExtField3& b) {

		T v1, vST[3];
		F2_ExtField_Double<F2_ExtField3> result;

		// res[0] = a[0]b[0]
		T::mulCombR2L_RegSize(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[4] = a[2]b[2]
		T::mulCombR2L_RegSize(result.high.xBF[1], a.xBF[2], b.xBF[2]);

		// v1 = a[1]b[1]
		T::mulCombR2L_RegSize(v1, a.xBF[1], b.xBF[1]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulCombR2L_RegSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulCombR2L_RegSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulCombR2L_RegSize(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v1;

		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= result.high.xBF[1];
		result.low.xBF[2] += v1;

		result.high.xBF[0] = vST[2] - v1;
		result.high.xBF[0] -= result.high.xBF[1];

		c.reduction(result);
	}

	static void mulLoadShift8(F2_ExtField3& c, const F2_ExtField3& a, const F2_ExtField3& b) {

		T v1, vST[3];
		F2_ExtField_Double<F2_ExtField3> result;

		// res[0] = a[0]b[0]
		T::mulLoadShift8(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[4] = a[2]b[2]
		T::mulLoadShift8(result.high.xBF[1], a.xBF[2], b.xBF[2]);

		// v1 = a[1]b[1]
		T::mulLoadShift8(v1, a.xBF[1], b.xBF[1]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulLoadShift8(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulLoadShift8(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulLoadShift8(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v1;

		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= result.high.xBF[1];
		result.low.xBF[2] += v1;

		result.high.xBF[0] = vST[2] - v1;
		result.high.xBF[0] -= result.high.xBF[1];

		c.reduction(result);
	}

	template<int wSize>
	static void mulCombL2RwithWindow(F2_ExtField3& c, const F2_ExtField3& a, const F2_ExtField3& b) {

		T v1, vST[3];
		F2_ExtField_Double<F2_ExtField3> result;

		// res[0] = a[0]b[0]
		T::template mulCombL2RwithWindow<wSize>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[4] = a[2]b[2]
		T::template mulCombL2RwithWindow<wSize>(result.high.xBF[1], a.xBF[2], b.xBF[2]);

		// v1 = a[1]b[1]
		T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[1], b.xBF[1]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v1;

		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= result.high.xBF[1];
		result.low.xBF[2] += v1;

		result.high.xBF[0] = vST[2] - v1;
		result.high.xBF[0] -= result.high.xBF[1];

		c.reduction(result);
	}

	template<int W>
	static void mulCombR2L(F2_ExtField3& c, const F2_ExtField3& a, const F2_ExtField3& b) {

		T v1, vST[3];
		F2_ExtField_Double<F2_ExtField3> result;

		// res[0] = a[0]b[0]
		T::template mulCombR2L<W>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[4] = a[2]b[2]
		T::template mulCombR2L<W>(result.high.xBF[1], a.xBF[2], b.xBF[2]);

		// v1 = a[1]b[1]
		T::template mulCombR2L<W>(v1, a.xBF[1], b.xBF[1]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::template mulCombR2L<W>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::template mulCombR2L<W>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::template mulCombR2L<W>(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v1;

		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= result.high.xBF[1];
		result.low.xBF[2] += v1;

		result.high.xBF[0] = vST[2] - v1;
		result.high.xBF[0] -= result.high.xBF[1];

		c.reduction(result);
	}

	/*

	(a_2w^2 + a_1w + a_0)^2 = (a_1^2 + a_2^2)w^2 + a_2^2w + a_0^2

	*/
	static void square(F2_ExtField3& c, const F2_ExtField3& a) {

		// (fix) c_2 = a_1^2
		T::square(c.xBF[2], a.xBF[1]);

		// c_1 = a_2^2
		T::square(c.xBF[1], a.xBF[2]);
		c.xBF[2] += c.xBF[1];

		// (fix) c_0 = a_0^2
		T::square(c.xBF[0], a.xBF[0]);
	}

	/*

	(a_2w^2 + a_1w + a_0) * w
	= a_1w^2 + (a_0 + a_2)w + a_2

	*/
	static void mulConstTerm(F2_ExtField3& c, const F2_ExtField3& a) {

		c.xBF[2] = a.xBF[1];
		c.xBF[0] = a.xBF[2];

		c.xBF[1] = a.xBF[2] + a.xBF[0];
	}
};

/*

extension field: $\mathbb F_{2^{6m}}$の定義
\mathbb F_{2^{6m}} = \mathbb F_{2^{3m}}[y]/(y^2+y+w+1)

*/
template<class T> class F2_ExtField3_2 {

public:

	static const int ext_degree = 6;
	static const int base_degree = T::ext_degree;
	static const int regLength = T::regLength;

	F2_ExtField3<T> xF3[2];



	F2_ExtField3_2() {}

#if 0
	F2_ExtField3_2(const F2_ExtField3<T>& a0, const F2_ExtField3<T>& a1)
		: xF3[0](a0)
		, xF3[1](a1)
	{}

	F2_ExtField3_2(const F2_ExtField3<T> *a)
		: xF3[0](a[0])
		, xF3[1](a[1])
	{}
#endif

	void clear() {

		xF3[0].clear();
		xF3[1].clear();
	}

	void clearUpDeg() {

		xF3[0].clearUpDeg();
		xF3[1].clearUpDeg();
	}

	void setRandom() {

		xF3[0].setRandom();
		xF3[1].setRandom();
	}

	void copyReg(const F2_ExtField3_2& a) {

		this->xF3[0].copyReg(a.xF3[0]);
		this->xF3[1].copyReg(a.xF3[1]);
	}

	void plusOne() {

		xF3[0].plusOne();
	}

	friend F2_ExtField3_2 operator+(const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3_2 c;

		c.xF3[0] = a.xF3[0] ^ b.xF3[0];
		c.xF3[1] = a.xF3[1] ^ b.xF3[1];
		return c;
	}

	friend F2_ExtField3_2 operator-(const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3_2 c;

		c.xF3[0] = a.xF3[0] ^ b.xF3[0];
		c.xF3[1] = a.xF3[1] ^ b.xF3[1];
		return c;
	}

	friend bool operator==(const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		bool result = true;

		result &= (a.xF3[0] == b.xF3[0]);
		result &= (a.xF3[1] == b.xF3[1]);
		return result;
	}

	F2_ExtField3_2 operator+=(const F2_ExtField3_2& b) {

		xF3[0] ^= b.xF3[0];
		xF3[1] ^= b.xF3[1];
		return *this;
	}

	F2_ExtField3_2 operator-=(const F2_ExtField3_2& b) {

		xF3[0] ^= b.xF3[0];
		xF3[1] ^= b.xF3[1];
		return *this;
	}

	/*

	reduction with defining polynomial s^2 + s + w + 1

	a_2s^2 + a_1s + a_0
	= (a_1 + a_2)s + w*a_2 + a_0 + a_2

	*/
	void reduction(const F2_ExtField_Double<F2_ExtField3_2>& c) {

		F2_ExtField3<T>::mulConstTerm(this->xF3[0], c.high.xF3[0]);
		this->xF3[0] += c.low.xF3[0];
		this->xF3[0] += c.high.xF3[0];

		this->xF3[1] = c.low.xF3[1] + c.low.xF3[2];
	}

	// Karatsuba
	static void mulShAdd(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T> vST;
		F2_ExtField_Double<F2_ExtField3_2> result;

		F2_ExtField3<T>::mulShAdd(result.low.xF3[0], a.xF3[0], b.xF3[0]);

		F2_ExtField3<T>::mulShAdd(result.high.xF3[0], a.xF3[1], b.xF3[1]);


		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		F2_ExtField3<T>::mulShAdd(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

		result.low.xF3[1] = vST - result.low.xF3[0];
		result.low.xF3[1] -= result.high.xF3[0];

		c.reduction(result);
	}

	static void mulCombR2L_RegSize(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T> vST;
		F2_ExtField_Double<F2_ExtField3_2> result;

		F2_ExtField3<T>::mulCombR2L_RegSize(result.low.xF3[0], a.xF3[0], b.xF3[0]);

		F2_ExtField3<T>::mulCombR2L_RegSize(result.high.xF3[0], a.xF3[1], b.xF3[1]);


		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		F2_ExtField3<T>::mulCombR2L_RegSize(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

		result.low.xF3[1] = vST - result.low.xF3[0];
		result.low.xF3[1] -= result.high.xF3[0];

		c.reduction(result);
	}

	static void mulLoadShift8(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T> vST;
		F2_ExtField_Double<F2_ExtField3_2> result;

		F2_ExtField3<T>::mulLoadShift8(result.low.xF3[0], a.xF3[0], b.xF3[0]);

		F2_ExtField3<T>::mulLoadShift8(result.high.xF3[0], a.xF3[1], b.xF3[1]);


		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		F2_ExtField3<T>::mulLoadShift8(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

		result.low.xF3[1] = vST - result.low.xF3[0];
		result.low.xF3[1] -= result.high.xF3[0];

		c.reduction(result);
	}

	template<int wSize>
	static void mulCombL2RwithWindow(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T> vST;
		F2_ExtField_Double<F2_ExtField3_2> result;

		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(result.low.xF3[0], a.xF3[0], b.xF3[0]);

		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(result.high.xF3[0], a.xF3[1], b.xF3[1]);


		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

		result.low.xF3[1] = vST - result.low.xF3[0];
		result.low.xF3[1] -= result.high.xF3[0];

		c.reduction(result);
	}

	template<int W>
	static void mulCombR2L(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {


		F2_ExtField3<T> vST;
		F2_ExtField_Double<F2_ExtField3_2> result;

		F2_ExtField3<T>::template mulCombR2L<W>(result.low.xF3[0], a.xF3[0], b.xF3[0]);

		F2_ExtField3<T>::template mulCombR2L<W>(result.high.xF3[0], a.xF3[1], b.xF3[1]);


		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		F2_ExtField3<T>::template mulCombR2L<W>(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

		result.low.xF3[1] = vST - result.low.xF3[0];
		result.low.xF3[1] -= result.high.xF3[0];

		c.reduction(result);
	}

	// for sparse multiplication $f\cdot\alpha\beta$ in Miller loop
	/*
	ab = (a0, a1)(b0, 0) = a0b0 + a1b0
	!!! オペランドの順に注意．$\alpha\beta$は右側 !!!
	*/
	static void mul_Sp_ShAdd(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {


		F2_ExtField3<T>::mulShAdd(c.xF3[0], a.xF3[0], b.xF3[0]);
		F2_ExtField3<T>::mulShAdd(c.xF3[1], a.xF3[1], b.xF3[0]);
	}
	static void mul_Sp_CombR2L_RegSize(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {


		F2_ExtField3<T>::mulCombR2L_RegSize(c.xF3[0], a.xF3[0], b.xF3[0]);
		F2_ExtField3<T>::mulCombR2L_RegSize(c.xF3[1], a.xF3[1], b.xF3[0]);
	}

	static void mul_Sp_LoadShift8(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T>::mulLoadShift8(c.xF3[0], a.xF3[0], b.xF3[0]);
		F2_ExtField3<T>::mulLoadShift8(c.xF3[1], a.xF3[1], b.xF3[0]);
	}

	template<int wSize>
	static void mul_Sp_CombL2RwithWindow(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(c.xF3[0], a.xF3[0], b.xF3[0]);
		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(c.xF3[1], a.xF3[1], b.xF3[0]);
	}

	template<int W>
	static void mul_Sp_CombR2L(F2_ExtField3_2& c, const F2_ExtField3_2& a, const F2_ExtField3_2& b) {

		F2_ExtField3<T>::template mulCombR2L<W>(c.xF3[0], a.xF3[0], b.xF3[0]);
		F2_ExtField3<T>::template mulCombR2L<W>(c.xF3[1], a.xF3[1], b.xF3[0]);
	}



	/*

	(a_1s + a_0)^2 = a_1^2s + (a_1^2w + a_1^2 + a_0^2)

	*/
	static void square(F2_ExtField3_2& c, const F2_ExtField3_2& a) {

		F2_ExtField3<T> tmp;

		F2_ExtField3<T>::square(c.xF3[1], a.xF3[1]);
		F2_ExtField3<T>::mulConstTerm(c.xF3[0], c.xF3[1]);
		c.xF3[0] += c.xF3[1];

		F2_ExtField3<T>::square(tmp, a.xF3[0]);
		c.xF3[0] += tmp;
	}

	/*

	(a_5s*w^2 + a_4s*w + a_3s  + a_2w^2 + a_1w + a_0)(s + s*w^2) = (a0 + a3)s*w^2 + (a2 + a5)s*w + (a0 + a1 + a3 + a4)s + (a3 + a5)w^2 + (a4 + a5)w + a4

	*/
	static void mulConstTerm(F2_ExtField3_2& c, const F2_ExtField3_2& a) {

		c.xF3[1].xBF[2] = a.xF3[0].xBF[0] + a.xF3[1].xBF[0];
		c.xF3[1].xBF[1] = a.xF3[0].xBF[2] + a.xF3[1].xBF[2];
		c.xF3[1].xBF[0] = a.xF3[0].xBF[0] + a.xF3[0].xBF[1];
		c.xF3[1].xBF[0] += a.xF3[1].xBF[0];
		c.xF3[1].xBF[0] += a.xF3[1].xBF[1];
		c.xF3[0].xBF[2] = a.xF3[1].xBF[0] + a.xF3[1].xBF[2];
		c.xF3[0].xBF[1] = a.xF3[1].xBF[2] + a.xF3[1].xBF[2];
		c.xF3[0].xBF[0] = a.xF3[1].xBF[2];
	}
};

/*

extension field: $\mathbb F_{2^{12m}}$の定義
\mathbb F_{2^{12m}} = \mathbb F_{2^{6m}}[z]/(z^2+z+s+sw^2)

*/
template<class T> class F2_ExtField3_2_2 {

public:

	static const int ext_degree = 12;
	static const int base_degree = T::ext_degree;
	static const int regLength = T::regLength;

	F2_ExtField3_2<T> xF3_2[2];



	F2_ExtField3_2_2() {}

#if 0
	F2_ExtField3_2_2(const F2_ExtField3_2<T>& a0, const F2_ExtField3_2<T>& a1)
		: xF3_2[0](a0)
		, xF3_2[1](a1)
	{}

	F2_ExtField3_2_2(const F2_ExtField3_2<T> *a)
		: xF3_2[0](a[0])
		, xF3_2[1](a[1])
	{}
#endif

	void clear() {

		xF3_2[0].clear();
		xF3_2[1].clear();
	}

	void clearUpDeg() {

		xF3_2[0].clearUpDeg();
		xF3_2[1].clearUpDeg();
	}

	void setRandom() {

		xF3_2[0].setRandom();
		xF3_2[1].setRandom();
	}

	void copyReg(const F2_ExtField3_2_2& a) {

		this->xF3_2[0].copyReg(a.xF3_2[0]);
		this->xF3_2[1].copyReg(a.xF3_2[1]);
	}

	void plusOne() {

		xF3_2[0].plusOne();
	}

	friend F2_ExtField3_2_2 operator+(const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField3_2_2 c;

		c.xF3_2[0] = a.xF3_2[0] ^ b.xF3_2[0];
		c.xF3_2[1] = a.xF3_2[1] ^ b.xF3_2[1];
		return c;
	}

	friend F2_ExtField3_2_2 operator-(const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField3_2_2 c;

		c.xF3_2[0] = a.xF3_2[0] ^ b.xF3_2[0];
		c.xF3_2[1] = a.xF3_2[1] ^ b.xF3_2[1];
		return c;
	}

	friend bool operator==(const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		bool result = true;

		result &= (a.xF3_2[0] == b.xF3_2[0]);
		result &= (a.xF3_2[1] == b.xF3_2[1]);
		return result;
	}

	F2_ExtField3_2_2 operator+=(const F2_ExtField3_2_2& b) {

		xF3_2[0] ^= b.xF3_2[0];
		xF3_2[1] ^= b.xF3_2[1];
		return *this;
	}

	F2_ExtField3_2_2 operator-=(const F2_ExtField3_2_2& b) {

		xF3_2[0] ^= b.xF3_2[0];
		xF3_2[1] ^= b.xF3_2[1];
		return *this;
	}

	/*

	reduction with defining polynomial t^2+z+t+sw^2

	a_2t^2 + a_1t + a_0
	= (a_1 + a_2)t + a_0 + (s*w^2 + s)*a_2

	*/
	void reduction(const F2_ExtField_Double<F2_ExtField3_2_2>& c) {

		F2_ExtField3_2<T>::mulConstTerm(this->xF3_2[0], c.high.xF3_2[0]);
		this->xF3_2[0] += c.low.xF3_2[0];
		this->xF3_2[1] = c.high.xF3_2[0] + c.low.xF3_2[1];
	}

	// Karatsuba
	static void mulShAdd(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulShAdd(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mulShAdd(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulShAdd(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	static void mulCombR2L_RegSize(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulCombR2L_RegSize(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mulCombR2L_RegSize(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulCombR2L_RegSize(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	static void mulLoadShift8(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulLoadShift8(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mulLoadShift8(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulLoadShift8(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	template<int wSize>
	static void mulCombL2RwithWindow(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	template<int W>
	static void mulCombR2L(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::template mulCombR2L<W>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::template mulCombR2L<W>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::template mulCombR2L<W>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// for sparse multiplication $f\cdot\alpha\beta$ in Miller loop
	/*
	ab = (a0, a1)(b0, b1), b1 = (*, 0). a1b1を F2_ExtField3_2::mul_Sp_* で行う
	!!! オペランドの順に注意．$\alpha\beta$は右側 !!!
	*/

	static void mulSh_Sp_Add(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulShAdd(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mul_Sp_ShAdd(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulShAdd(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	static void mul_Sp_CombR2L_RegSize(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulCombR2L_RegSize(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mul_Sp_CombR2L_RegSize(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulCombR2L_RegSize(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	static void mul_Sp_LoadShift8(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::mulLoadShift8(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::mul_Sp_LoadShift8(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::mulLoadShift8(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	template<int wSize>
	static void mul_Sp_CombL2RwithWindow(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::template mul_Sp_CombL2RwithWindow<wSize>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}

	// Karatsuba
	template<int W>
	static void mul_Sp_CombR2L(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2_2& b) {

		F2_ExtField_Double<F2_ExtField3_2_2> result;

		F2_ExtField3_2<T>::template mulCombR2L<W>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
		F2_ExtField3_2<T>::template mul_Sp_CombR2L<W>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

		F2_ExtField3_2<T>::template mulCombR2L<W>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
		result.low.xF3_2[1] -= result.low.xF3_2[0];
		result.low.xF3_2[1] -= result.high.xF3_2[0];

		c.reduction(result);
	}



	/*

	(a_1t + a_0)^2 = a_1^2t + a_1^2(s*w^2 + s) + a_0^2

	*/
	static void square(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a) {

		F2_ExtField3_2<T> tmp;

		F2_ExtField3_2<T>::square(c.xF3_2[1], a.xF3_2[1]);
		F2_ExtField3_2<T>::mulConstTerm(c.xF3_2[0], c.xF3_2[1]);

		F2_ExtField3_2<T>::square(tmp, a.xF3_2[0]);
		c.xF3_2[0] += tmp;
	}


	// \alpha\beta
	/*

	!! 乗算はregSizeのComb methodで行う !!
	(a0,a1,a2,a3,a4,a5) = a0 + a1w+ a2w^2 + a3s + a4sw + a5sw^2とする

	a=(a0,a1,a2,c,0,0, (1,0,0,0,0,0)) = (f,1)
	b=(b0,b1,b2,c,0,0, (1,0,0,0,0,0)) = (g,1)
	==> ab = (fg,f+g,1) = (fg + s + sw^2, f + g + 1)
           = (f + g + 1)z + fg + s + sw^2

	d=c^2, dはペアリングアルゴリズムの事前計算で求められた値を使えるため引数として渡す

	fg = (a0,a1,a2,c,0,0)(b0,b1,b2,c,0,0)
	   = (a0,a1,a2)(b0,b1,b2) + ( c((a0,a1,a2) + (b0,b1,b2)) )y + (d=c^2,0,0)y^2
	   = (a0,a1,a2)(b0,b1,b2) + (d,d,0) + ( c((a0,a1,a2) + (b0,b1,b2)) + d )y (mod y^2+y+w+1)

	(a0,a1,a2)(b0,b1,b2)をF2_ExtField3の乗算 (Karatusba 3) を用いて計算する

	*/

	static void alpha_beta(F2_ExtField3_2_2& c, const F2_ExtField3_2<T>& a, const F2_ExtField3_2<T>& b, const T& d) {

		/* コストはK3+3M+11A (F2_ExtField3でのmod演算による加算等も含む) */
		F2_ExtField3_2<T> fg;
		fg.clear();

		F2_ExtField3<T> tmpF3;

		// c := (c_0,c_1,c_2,c_3)

		// fg
		// c_0 := (a0,a1,a2)(b0,b1,b2)
		F2_ExtField3<T>::mulCombR2L_RegSize(fg.xF3[0], a.xF3[0], b.xF3[0]);
		fg.xF3[0].xBF[0] += d;
		fg.xF3[0].xBF[1] += d;

		tmpF3 = a.xF3[0] + b.xF3[0];

		// (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
		c.xF3_2[1].xF3[0] = tmpF3;
		c.xF3_2[1].xF3[1].clear();

		// (fix) (c_2,c_3) = f + g + 1
		c.xF3_2[1].plusOne();

		// tmpF3 = (a0+b0,a1+b1,a2+b2)
		T::mulCombR2L_RegSize(fg.xF3[1].xBF[0], a.xF3[1].xBF[0], tmpF3.xBF[0]);
		T::mulCombR2L_RegSize(fg.xF3[1].xBF[1], a.xF3[1].xBF[0], tmpF3.xBF[1]);
		T::mulCombR2L_RegSize(fg.xF3[1].xBF[2], a.xF3[1].xBF[0], tmpF3.xBF[2]);

		fg.xF3[1].xBF[0] += d;

		//(c_0,c_1) = fg + s + sw^2
		c.xF3_2[0] = fg;
		c.xF3_2[0].xF3[1].xBF[0].plusOne();
		c.xF3_2[0].xF3[1].xBF[2].plusOne();
	}

	static void alpha_betaLoadShift8(F2_ExtField3_2_2& c, const F2_ExtField3_2<T>& a, const F2_ExtField3_2<T>& b, const T& d) {

		/* コストはK3+3M+11A (F2_ExtField3でのmod演算による加算等も含む) */
		F2_ExtField3_2<T> fg;
		fg.clear();

		F2_ExtField3<T> tmpF3;

		// c := (c_0,c_1,c_2,c_3)

		// fg
		// c_0 := (a0,a1,a2)(b0,b1,b2)
		F2_ExtField3<T>::mulLoadShift8(fg.xF3[0], a.xF3[0], b.xF3[0]);
		fg.xF3[0].xBF[0] += d;
		fg.xF3[0].xBF[1] += d;

		tmpF3 = a.xF3[0] + b.xF3[0];

		// (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
		c.xF3_2[1].xF3[0] = tmpF3;
		c.xF3_2[1].xF3[1].clear();

		// (fix) (c_2,c_3) = f + g + 1
		c.xF3_2[1].plusOne();

		// tmpF3 = (a0+b0,a1+b1,a2+b2)
		T::mulLoadShift8(fg.xF3[1].xBF[0], a.xF3[1].xBF[0], tmpF3.xBF[0]);
		T::mulLoadShift8(fg.xF3[1].xBF[1], a.xF3[1].xBF[0], tmpF3.xBF[1]);
		T::mulLoadShift8(fg.xF3[1].xBF[2], a.xF3[1].xBF[0], tmpF3.xBF[2]);

		fg.xF3[1].xBF[0] += d;

		//(c_0,c_1) = fg + s + sw^2
		c.xF3_2[0] = fg;
		c.xF3_2[0].xF3[1].xBF[0].plusOne();
		c.xF3_2[0].xF3[1].xBF[2].plusOne();
	}

	template<int wSize>
	static void alpha_beta_withWindow(F2_ExtField3_2_2& c, const F2_ExtField3_2<T>& a, const F2_ExtField3_2<T>& b, const T& d) {

		/* コストはK3+3M+11A (F2_ExtField3でのmod演算による加算等も含む) */
		F2_ExtField3_2<T> fg;
		fg.clear();

		F2_ExtField3<T> tmpF3;

		// c := (c_0,c_1,c_2,c_3)

		// fg
		// c_0 := (a0,a1,a2)(b0,b1,b2)
		F2_ExtField3<T>::template mulCombL2RwithWindow<wSize>(fg.xF3[0], a.xF3[0], b.xF3[0]);
		fg.xF3[0].xBF[0] += d;
		fg.xF3[0].xBF[1] += d;

		tmpF3 = a.xF3[0] + b.xF3[0];

		// (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
		c.xF3_2[1].xF3[0] = tmpF3;
		c.xF3_2[1].xF3[1].clear();

		// (fix) (c_2,c_3) = f + g + 1
		c.xF3_2[1].plusOne();

		// tmpF3 = (a0+b0,a1+b1,a2+b2)
		T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[0], a.xF3[1].xBF[0], tmpF3.xBF[0]);
		T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[1], a.xF3[1].xBF[0], tmpF3.xBF[1]);
		T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[2], a.xF3[1].xBF[0], tmpF3.xBF[2]);

		fg.xF3[1].xBF[0] += d;

		//(c_0,c_1) = fg + s + sw^2
		c.xF3_2[0] = fg;
		c.xF3_2[0].xF3[1].xBF[0].plusOne();
		c.xF3_2[0].xF3[1].xBF[2].plusOne();
	}

	/*

	final addition / doublings におけるoperandの一方がsparseな元の乗算
	c = aB; a: normal, B: sparse
	B = (b,1) = b + t
	aB = (a0,a1)(b,1) = (a0 + a1(b+1))z + a0b + a1(s + sw^2)

	!! 乗算はregSizeのComb methodで行う !!

	*/
	static void sparseMul(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField3_2<T> tmp;

		// c_0 = b + 1
		c.xF3_2[0] = b;
		c.xF3_2[0].xF3[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField3_2<T>::mulCombR2L_RegSize(c.xF3_2[0], a.xF3_2[1], c.xF3_2[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF3_2[1] += a.xF3_2[0];



		// tmp = a1(s + sw^2)
		F2_ExtField3_2<T>::mulConstTerm(tmp, a.xF3_2[1]);
		// c_0 = a0b
		F2_ExtField3_2<T>::mulCombR2L_RegSize(c.xF3_2[0], a.xF3_2[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF3_2[0] += tmp;
	}

	static void sparseMulLoadShift8(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField3_2<T> tmp;

		// c_0 = b + 1
		c.xF3_2[0] = b;
		c.xF3_2[0].xF3[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField3_2<T>::mulLoadShift8(c.xF3_2[0], a.xF3_2[1], c.xF3_2[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF3_2[1] += a.xF3_2[0];



		// tmp = a1(s + sw^2)
		F2_ExtField3_2<T>::mulConstTerm(tmp, a.xF3_2[1]);
		// c_0 = a0b
		F2_ExtField3_2<T>::mulLoadShift8(c.xF3_2[0], a.xF3_2[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF3_2[0] += tmp;
	}

	template<int wSize>
	static void sparseMul_withWindow(F2_ExtField3_2_2& c, const F2_ExtField3_2_2& a, const F2_ExtField3_2<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField3_2<T> tmp;

		// c_0 = b + 1
		c.xF3_2[0] = b;
		c.xF3_2[0].xF3[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(c.xF3_2[0], a.xF3_2[1], c.xF3_2[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF3_2[1] += a.xF3_2[0];



		// tmp = a1(s + sw^2)
		F2_ExtField3_2<T>::mulConstTerm(tmp, a.xF3_2[1]);
		// c_0 = a0b
		F2_ExtField3_2<T>::template mulCombL2RwithWindow<wSize>(c.xF3_2[0], a.xF3_2[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF3_2[0] += tmp;
	}
};
