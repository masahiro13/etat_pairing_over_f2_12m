#pragma once

#include <stdint.h>

#include <string>
#include <random>

#ifdef _MSC_VER
  #include <emmintrin.h>
  #include <smmintrin.h>
#else
  #include <x86intrin.h>
#endif

#include "Standard_char2.h"

struct char2_128 {

	static const int regLength = 128;

	__m128i x;



	char2_128() {}
	char2_128(__m128i a) : x(a) {}
	char2_128(uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3) : x( _mm_set_epi32(a3, a2, a1, a0) ) {}
	//char2_128(const char2_128& a) : x(a.x) {}

	My_Force_Inline void clear() {
		*this = _mm_setzero_si128();
	}

	My_Force_Inline void set32(uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3) {

		this->x = _mm_set_epi32(a3, a2, a1, a0);
	}

	My_Force_Inline void set32(uint32_t *a) {

		this->x = _mm_set_epi32(a[3], a[2], a[1], a[0]);
	}

	My_Force_Inline void set_all_1bit() {

		set32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
	}

	My_Force_Inline void setString(const char *str) {

		My_Align(16) uint32_t a[4];
		//__declspec(align(16)) uint32_t a[4];
		setString32<4>(a, str);
		set32(a[0], a[1], a[2], a[3]);
	}

	My_Force_Inline void setStringNoChk(const char *str) {

		My_Align(16) uint32_t a[4];
		//__declspec(align(16)) uint32_t a[4];
		setString32NoChk<4>(a, str);
		set32(a[0], a[1], a[2], a[3]);
	}

	/*position番目のbitを1にする*/
	My_Force_Inline void setBit1(int position) {

		assert(0 <= position && position < regLength);
		char2_128 tmp;
		My_Align(16) uint32_t a32[4];
		//__declspec(align(16)) uint32_t a32[4];
		for (unsigned int i = 0; i < 4; ++i) a32[i] = 0;

		int pos32 = position / 32;
		int posRem = position - 32 * pos32;
		a32[pos32] = 1;
		a32[pos32] <<= posRem;

		tmp.set32(a32);
		*this |= tmp;
	}

	My_Force_Inline void setRandom() {

		My_Align(16) uint32_t a[4];
		//__declspec(align(16)) uint32_t a[4];
		std::random_device rd;
		std::mt19937 mt(rd());

		a[0] = mt();
		a[1] = mt();
		a[2] = mt();
		a[3] = mt();
		this->set32(a[3], a[2], a[1], a[0]);
	}

	My_Force_Inline void store(uint32_t *a) const {

		_mm_storeu_si128((__m128i*)a, x);
		//*(__m128i*)a = x;
	}

	My_Force_Inline void store64(uint64_t *a) const {

		_mm_storeu_si128((__m128i*)a, x);
		//*(__m128i*)a = x;
	}

	My_Force_Inline void load(const uint32_t *a) {

		x = _mm_loadu_si128((__m128i*)a);
		//x = *(const __m128i*)a;
	}

	My_Force_Inline uint32_t getBit(const uint32_t position) {

		assert(0 <= position && position < 128);
		My_Align(16) uint32_t x32[4];
		//__declspec(align(16)) uint32_t x32[4];

		store(x32);
		return getBit32(x32, position);
	}

	My_Force_Inline std::string getStringAll() const {

		std::string str;
		My_Align(16) uint32_t x32[4];
		//__declspec(align(16)) uint32_t x32[4];

		store(x32);
		for (int i = 127; i >= 0; --i) {
			str += static_cast<char>(getBit32(x32, i) + '0');
		}
		return str;
	}

	// i番目の文字がposition iのbit
	My_Force_Inline std::string getStringAll_R() const {

		std::string str;
		My_Align(16) uint32_t x32[4];
		//__declspec(align(16)) uint32_t x32[4];

		store(x32);
		for (int i = 0; i < 128; ++i) {
			str += static_cast<char>(getBit32(x32, i) + '0');
		}
		return str;
	}

	My_Force_Inline std::string getString() {

		std::string str;
		My_Align(16) uint32_t x32[4];
		//__declspec(align(16)) uint32_t x32[4];

		store(x32);
		for (int i = 127; i >= 0; --i) {
			str += static_cast<char>(getBit32(x32, i) + '0');
			if ( (i > 0) && ((i & 31) == 0) ) str += ' ';
		}

		rmTop0(str);
		return str;
	}

	bool testZero() const;

	My_Force_Inline friend char2_128 operator&(const char2_128& a, const char2_128& b) {

		char2_128 c(_mm_and_si128(a.x, b.x));
		return c;
	}
	My_Force_Inline friend char2_128 operator|(const char2_128& a, const char2_128& b) {

		char2_128 c(_mm_or_si128(a.x, b.x));
		return c;
	}

	My_Force_Inline friend char2_128 operator^(const char2_128& a, const char2_128& b) {

		char2_128 c(_mm_xor_si128(a.x, b.x));
		return c;
	}

	My_Force_Inline friend bool operator==(const char2_128& a, const char2_128& b) {

		__m128i cmp;
		cmp = _mm_cmpeq_epi64(a.x, b.x);
		return ( _mm_test_all_ones(cmp) != 0 );
	}

	My_Force_Inline char2_128& operator&=(const char2_128& b) {
		x = _mm_and_si128(x, b.x);
		return *this;
	}

	My_Force_Inline char2_128& operator|=(const char2_128& b) {
		x = _mm_or_si128(x, b.x);
		return *this;
	}

	My_Force_Inline char2_128& operator^=(const char2_128& b) {
		x = _mm_xor_si128(x, b.x);
		return *this;
	}








	template<int n> My_Force_Inline void shiftLeftBit();
	template<int n> My_Force_Inline void shiftRightBit();

	template<int n> static My_Force_Inline char2_128 shiftLeftBit(const char2_128&);
	template<int n> static My_Force_Inline char2_128 shiftRightBit(const char2_128&);

	My_Force_Inline void shiftLeftBit1();
    My_Force_Inline void shiftRightBit1();

	static My_Force_Inline char2_128 shiftLeftBit1(const char2_128&);
	static My_Force_Inline char2_128 shiftRightBit1(const char2_128&);

	My_Force_Inline void shiftLeftBitR_1();
    My_Force_Inline void shiftRightBitR_1();

	static My_Force_Inline char2_128 shiftLeftBitR_1(const char2_128&);
	static My_Force_Inline char2_128 shiftRightBitR_1(const char2_128&);

	//nB bytes shift
	template<int nB> My_Force_Inline void shiftLeftByte();
	template<int nB> My_Force_Inline void shiftRightByte();

	template<int nB> static My_Force_Inline char2_128 shiftLeftByte(const char2_128&);
	template<int nB> static My_Force_Inline char2_128 shiftRightByte(const char2_128&);

};

static My_Force_Inline char2_128 zero() {

	return char2_128(_mm_setzero_si128());
}

My_Force_Inline bool char2_128::testZero() const {

	return _mm_testz_si128(x, x);

	//char2_128 mask;
	//mask.set_all_1bit();
	//return ( _mm_testz_si128(this->x, mask.x) != 0 );
}

#if 1
template<int n>
static My_Force_Inline char2_128 pslldq(const char2_128& a) {

	return _mm_slli_si128(a.x, n);
}

template<int n>
static My_Force_Inline char2_128 psrldq(const char2_128& a) {

	return _mm_srli_si128(a.x, n);
}

template<int n>
static My_Force_Inline char2_128 psllq(const char2_128& a) {

	return _mm_slli_epi64(a.x, n);
}

template<int n>
static My_Force_Inline char2_128 psrlq(const char2_128& a) {

	return _mm_srli_epi64(a.x, n);
}
#else
template<int n>
static My_Force_Inline char2_128 pslldq(const char2_128& a) {

	char2_128 c(_mm_slli_si128(a.x, n));
	return c;
}

template<int n>
static My_Force_Inline char2_128 psrldq(const char2_128& a) {

	char2_128 c(_mm_srli_si128(a.x, n));
	return c;
}

template<int n>
static My_Force_Inline char2_128 psllq(const char2_128& a) {

	char2_128 c(_mm_slli_epi64(a.x, n));
	return c;
}

template<int n>
static My_Force_Inline char2_128 psrlq(const char2_128& a) {

	char2_128 c(_mm_srli_epi64(a.x, n));
	return c;
}
#endif

template<int n>
My_Force_Inline void char2_128::shiftLeftBit() {

	if (n < 64) {

		*this = psllq<n>(*this) | psrlq<64 - n>(pslldq<8>(*this));
		return;
	}
	else if (n == 64) {

		*this = pslldq<8>(*this);
		return;
	}
	else if (n > 64) {

		*this = psllq<n - 64>(pslldq<8>(*this));
		return;
	}
}

template<int n>
My_Force_Inline void char2_128::shiftRightBit() {

	if (n < 64) {

		*this = psrlq<n>(*this) | psllq<64 - n>(psrldq<8>(*this));
		return;
	}
	else if (n == 64) {

		*this = psrldq<8>(*this);
		return;
	}
	else if (n > 64) {

		*this = psrlq<n - 64>(psrldq<8>(*this));
		return;
	}
}

template<int n>
My_Force_Inline char2_128 char2_128::shiftLeftBit(const char2_128& a) {

	char2_128 c;

	if (n < 64) {

		c = psllq<n>(a) | psrlq<64 - n>(pslldq<8>(a));
		return c;
	}
	else if (n == 64) {

		c = pslldq<8>(a);
		return c;
	}
	else if (n > 64) {

		c = psllq<n - 64>(pslldq<8>(a));
		return c;
	}

}

template<int n>
My_Force_Inline char2_128 char2_128::shiftRightBit(const char2_128& a) {

	char2_128 c;

	if (n < 64) {

		c = psrlq<n>(a) | psllq<64 - n>(psrldq<8>(a));
		return c;
	}
	else if (n == 64) {

		c = psrldq<8>(a);
		return c;
	}
	else if (n > 64) {

		c = psrlq<n - 64>(psrldq<8>(a));
		return c;
	}

}

My_Force_Inline void char2_128::shiftLeftBit1() {

	*this = psllq<1>(*this) | psrlq<63>(pslldq<8>(*this));
	return;	
}

My_Force_Inline void char2_128::shiftRightBit1() {

	*this = psrlq<1>(*this) | psllq<63>(psrldq<8>(*this));
	return;
}

My_Force_Inline char2_128 char2_128::shiftLeftBit1(const char2_128& a) {

	char2_128 c;
	c = a;
	c.shiftLeftBit1();
	return c;
}

My_Force_Inline char2_128 char2_128::shiftRightBit1(const char2_128& a) {

	char2_128 c;
	c = a;
	c.shiftRightBit1();
	return c;
}

// reglength - 1 (>64) bit shift
My_Force_Inline void char2_128::shiftLeftBitR_1() {

	*this = psllq<63>(pslldq<8>(*this));
	return;
}

My_Force_Inline void char2_128::shiftRightBitR_1() {

	*this = psrlq<63>(psrldq<8>(*this));
	return;
}

My_Force_Inline char2_128 char2_128::shiftLeftBitR_1(const char2_128& a) {

	char2_128 c;
	c = a;
	c.shiftLeftBitR_1();
	return c;
}

My_Force_Inline char2_128 char2_128::shiftRightBitR_1(const char2_128& a) {

	char2_128 c;
	c = a;
	c.shiftRightBitR_1();
	return c;
}

template<int nB>
My_Force_Inline void char2_128::shiftLeftByte() {

	*this = pslldq<nB>(*this);
}

template<int nB>
My_Force_Inline void char2_128::shiftRightByte() {

	*this = psrldq<nB>(*this);
}

template<int nB>
My_Force_Inline char2_128 char2_128::shiftLeftByte(const char2_128& a) {

	char2_128 c;

	c = pslldq<nB>(a);
	return c;
}

template<int nB>
My_Force_Inline char2_128 char2_128::shiftRightByte(const char2_128& a) {

	char2_128 c;

	c = psrldq<8>(a);
	return c;
}

#if 0
My_Force_Inline __m128i operator&(const __m128i& a, const __m128i& b) {

	return _mm_and_si128(a, b);
}

My_Force_Inline __m128i operator|(const __m128i& a, const __m128i& b) {

	return _mm_or_si128(a, b);
}

My_Force_Inline __m128i operator^(const __m128i& a, const __m128i& b) {

	return _mm_xor_si128(a, b);
}
#endif