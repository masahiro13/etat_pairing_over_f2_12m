#pragma once

#include <string.h>
#include <assert.h>

#include "Environment.h"

#define LOOP4(var, exp)\
{\
	unsigned int var = 0;\
	exp;\
	var = 1;\
	exp;\
	var = 2;\
	exp;\
	var = 3;\
	exp;\
}

#define LOOP_N(_type, _var, _count, _express)\
{\
	assert(_count <= 8);\
	_type _var = 0;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
	_var++;\
	if (_var < _count) { _express; }\
}

/* for readability */
#define LOOP_down_N(_type, _var, _max, _count, _express)\
{\
	assert(_count <= 8);\
	_type _var = _max;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
	--_var;\
	if (_max - _var < _count) { _express; }\
}

static inline void removeSpace(std::string& noSpaceStr, const char *str) {

	noSpaceStr = "";
	int len = static_cast<int>( strlen(str) );
	for (int i = 0; i < len; ++i) {
		if (str[i] != ' ') noSpaceStr += str[i];
	}
}

// assume no space in string
template<int n32>
static inline void setString32NoChk(uint32_t(&a)[n32], const char *str) {

	for (unsigned int i = 0; i < n32; ++i) a[i] = 0;

	int len = static_cast<int>( strlen(str) );
	int position = len - 1;
	int bit;

	for (unsigned int i = 0; i < n32; ++i) {
		for (unsigned int j = 0; j < 32; ++j, --position) {

			if (position < 0) return;

			bit = str[position] - '0';
			assert(bit < 2);
			a[i] |= bit << j;
		}
	}
}

template<int n32> 
static inline void setString32(uint32_t(&a)[n32], const char *str) {

	std::string noSpaceStr;
	removeSpace(noSpaceStr, str);

	setString32NoChk(a, noSpaceStr.c_str());
}

#if 0
template<int n32> 
static inline void setString32(uint32_t(&a)[n32], const char *str) {

	for (unsigned int i = 0; i < n32; ++i) a[i] = 0;

	int len = strlen(str);
	int position = len - 1;
	uint32_t bit;

	for (unsigned int i = 0; i < n32; ++i) {
		for (unsigned int j = 0; j < 32;) {

			if (position < 0) return;
			if (str[position] != ' ') {

				bit = str[position] - '0';
				assert(bit < 2);
				a[i] |= bit << j;

				++j;
			}
			--position;
		}
	}
}
#endif
static inline uint32_t get1bit32(const uint32_t a, const int position) {

	assert(position < 32);
	return (a >> position) & 1;
}

static inline uint32_t getBit32(const uint32_t *a, const uint32_t position) {

	uint32_t n32 = position / 32;
	uint32_t pos32 = position -  32 * n32;

	return (a[n32] >> pos32) & 1;
	//return get1bit32(a[n32], pos32);
}

static inline void rmTop0(std::string& str) {

	int position = 0;
	while (str[position] == '0') ++position;

	str.erase(0, position);
}

static inline void formatStr(std::string& str) {

	rmTop0(str);

	int len = static_cast<int>( str.length() );
	int nSpace;

	if (len <= 32) return;
	else nSpace = (len - 1) / 32;

	for (int i = 0; i < nSpace; ++i) {
		str.insert(len - (32 + i * 32), " ");
	}
}

static inline int address_st(int s, int t, int size) {

  int address = t - 1 - s; // s行のアドレスが全て0,...,size-2-iになる．

  // s行の個数はsize - 1 - s.
  for (; s > 0; --s) address += size - s;

  return address;

}

// addr = (row,column,sizeDeg): row: 0, column: 1
// (4,5,6) <---> 14 のとき address_st_inv(14,0,6) = 4 (row), address_st_inv(14,1,6) = 5 (column) などを求める函数．
static inline int address_st_inv(int addr, int pos, int sizeDeg) {

  int s = 0;
  int t = 1;
  int tmp = 0; // s行の最小アドレス

  for (; s < sizeDeg; ++s) {

    if (addr < tmp + sizeDeg - s - 1) {
      t = addr - tmp + s + 1;
      break;
    }
    tmp += sizeDeg - s - 1;
  }

  if (pos == 0) return s;
  else return t;

}

template<int n>
struct power2 {

	static const int num = 2 * power2<n-1>::num;
};

template<>
struct power2<0> {

	static const int num = 1;
};

template<int wSize>
static inline int index(const uint32_t *arr, const int position) {

	const int pos32 = position / 32;
	//const int rem32 = position % 32;
	const int rem32 = position - 32 * pos32;
	//const int end_index = (rem32 + wSize - 1) % 32;
	int tmp = rem32 + wSize - 1;
	int end_index;
	if (tmp >= 32) end_index = tmp - 32; /* !!! wSizeは32以下の必要がある !!! */
	else end_index = tmp;

	if (tmp < 32) return (arr[pos32] << 31 - end_index) >> 31 - wSize + 1;
	else return (arr[pos32] >> rem32) + ((arr[pos32 + 1] << 31 - end_index) >> rem32 - end_index - 1);
}

template<int wSize>
static inline int index64(const uint64_t *arr, const int position) {

	const unsigned int pos64 = position / 64;
	const unsigned int rem64 = position % 64;
	const unsigned int tmp = rem64 + wSize - 1;
	const unsigned int end_index = tmp % 64;

	if (tmp < 64) return (arr[pos64] << 63 - end_index) >> 63 - wSize + 1;
	else return (arr[pos64] >> rem64) + ((arr[pos64 + 1] << 63 - end_index) >> rem64 - end_index - 1);
}

#if 0
static const int power2Arr[11] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024 };

template<int wSize>
static inline int index(const std::string& str, int position) {

	int address = 0;
	for (int i = 0; i < wSize; ++i) {
		if (str[position + i] == '1') address += power2Arr[i];
	}

	return address;
}
#endif