#pragma once

#include "base_field.h"

// $\mathbb F_{2^511}$ \simeq \mathbb F_2[x]/(x^511+x^10+1)
// ceil(511/32) = 16
// ceil(511/128) = 4

typedef F2_BaseField<char2_128, 511> F2_511_r128;



/*********************************************************************************************/
/*********************************************************************************************/

#if 0
template<int Max>
struct callMeta_ShiftLeftBitF2_511_r128 {

	void operator()(F2_511_r128& result, int n, const F2_511_r128& a) {

		if (Max == n) result = F2_511_r128::shiftLeftBit<Max>(a);
		else callMeta_ShiftLeftBitF2_511_r128<Max-1>()(result, n, a);
	}

};

template<>
struct callMeta_ShiftLeftBitF2_511_r128<0> {
	void operator()(F2_511_r128& result, int n, const F2_511_r128& a) {

		result = F2_511_r128::shiftLeftBit<0>(a);
	}
};



template<int Max>
struct callMeta_ShiftRightBitF2_511_r128 {

	void operator()(F2_511_r128& result, int n, const F2_511_r128& a) {

		if (Max == n) result = F2_511_r128::shiftRightBit<Max>(a);
		else callMeta_ShiftRightBitF2_511_r128<Max-1>()(result, n, a);
	}

};

template<>
struct callMeta_ShiftRightBitF2_511_r128<0> {
	void operator()(F2_511_r128& result, int n, const F2_511_r128& a) {

		result = F2_511_r128::shiftRightBit<0>(a);
	}
};
#endif

/*********************************************************************************************/
/*********************************************************************************************/

//reduction with difining polynomial x^511 + x^10 + 1
template<>
My_Force_Inline void F2_BaseField_Double<F2_511_r128>::reduction(F2_511_r128 & c) {

	F2_511_r128 tmp;
	//F2_511_r128 tmp2;
	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);
	c.copyReg(this->low);

#if 1

	c += this->high;
	c += F2_511_r128::shiftLeftBit<10>(this->high);

	/* 関数内で演算結果を参照渡しの方が低速の場合も */
	tmp = F2_511_r128::shiftRightBit<501>(this->high);
	//F2_511_r128::shiftRightBit<501>(tmp, this->high);

	c += tmp;

	c += F2_511_r128::shiftLeftBit<10>(tmp);
	//F2_511_r128::shiftLeftBit<10>(tmp2, tmp);
	//c += tmp2;

	c.xReg[3] &= mask128;

#else
	c += this->high;
	c += F2_511_r128::shiftLeftBit<10>(this->high);
	c += F2_511_r128::shiftRightBit<501>(this->high);
	c += F2_511_r128::shiftLeftBit<10>( F2_511_r128::shiftRightBit<501>(this->high) );
	c.xReg[3] &= mask128;
#endif
}

template<>
My_Force_Inline void F2_511_r128::mulShAdd(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {

//#if 0
	if (a.testZero() || b.testZero()) {

		c.clearReg();
		return;
	}
//#endif

	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	F2_511_r128 tmp;
	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];
	a.copyTo32(arr32);

	if (getBit32(arr32, 0) == 1) result.low.copyReg(b);

#if 1

	F2_511_r128 bL, bH;
	bL = b;
	bH = b;
	bL.shiftLeftBit1();
	for (int i = 1; i < 511; ++i) {

		if ( getBit32(arr32, i) == 1 ) result.low ^= bL;
		bL.shiftLeftBit1();
	}

	bH.shiftRightBit1();
	for (int i = 510; i >= 1; --i) {

		if ( getBit32(arr32, i) == 1 ) result.high ^= bH;
		bH.shiftRightBit1();
	}

#else
	for (int i = 1; i < 511; ++i) {

		if ( getBit32(arr32, i) == 1 ) {

			callMeta_ShiftLeftBitF2_511_r128<510>()(tmp, i, b); /* result[0]の512番目のビット情報は無視しなければならない */
			result.low ^= tmp;

			callMeta_ShiftRightBitF2_511_r128<510>()(tmp, 511 - i, b);
			result.high ^= tmp;
		}
	}
#endif

	//result.clearUpDeg();
	//result.low.clearUpDeg();

	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);
	result.low.xReg[3] &= mask128;

	result.reduction(c);
}

template<>
My_Force_Inline void F2_511_r128::mulCombR2L_RegSize(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {

//#if 0
	if (a.testZero() || b.testZero()) {

		c.clearReg();
		return;
	}
//#endif

	static const unsigned int nW = (511 + F2_511_r128::regLength - 1) / F2_511_r128::regLength;

	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	//F2_511_r128 tmpL, tmpR;

	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];
	a.copyTo32(arr32);

#if 1

	F2_511_r128 bL, bH ,tmpBF;
	bL = b;
	bH = b;

	for (int i = 0; i < F2_511_r128::regLength; ++i) {

		for (int j = 0; j < nW; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			if (getBit32(arr32, i + (j * F2_511_r128::regLength)) == 1) {

				//result.low ^= F2_511_r128::shiftLeftRegSize(bL, j);
				F2_511_r128::shiftLeftRegSize(tmpBF, bL, j);
				result.low ^= tmpBF;
			}
		}

		bL.shiftLeftBit1();
	}

	/* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
	/* m=511のときはシフトする必要はない (bH) */
	for (int i = F2_511_r128::regLength - 1; i >= 0; --i) {

		for (int j = 0; j < nW; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			if (getBit32(arr32, i + (j * F2_511_r128::regLength)) == 1) {

				//result.high ^= F2_511_r128::shiftRightRegSize(bH, nW - j - 1);
				F2_511_r128::shiftRightRegSize(tmpBF, bH, nW - j - 1);
				result.high ^= tmpBF;
			}
		}

		bH.shiftRightBit1();
	}

#else

	for (int i = 0; i < F2_511_r128::regLength; ++i) {

		callMeta_ShiftLeftBitF2_511_r128<127>()(tmpL, i, b);
		callMeta_ShiftRightBitF2_511_r128<127>()(tmpR, 127 - i, b);
		for (int j = 0; j < nW; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			if ( getBit32(arr32, i + (j * F2_511_r128::regLength) ) == 1) {

				result.low ^= F2_511_r128::shiftLeftRegSize(tmpL, j);
				result.high ^= F2_511_r128::shiftRightRegSize(tmpR, nW - j - 1);
			}
		}
	}

#endif

	//result.clearUpDeg();
	//result.low.clearUpDeg();

	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);
	result.low.xReg[3] &= mask128;

	result.reduction(c);
}

/* !!! WはregSize * nR (= 512) の約数である必要がある !!! */
template<>
template<int W>
My_Force_Inline void F2_511_r128::mulCombR2L(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {

	/* バイト単位シフトで作る...どうしてもtemplate meta方式でやるしかないか... */

	F2_511_r128::mulCombR2L_RegSize(c, a, b);
	return;

#if 0 // template meta

	if (a.testZero() || b.testZero()) {

		c.clearReg();
		return;
	}

	static const int nW = (511 + W - 1) / W;
	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	F2_511_r128 tmpL, tmpR, tmp;

	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];
	a.copyTo32(arr32);


	for (int i = 0; i < W; ++i) {

		callMeta_ShiftLeftBitF2_511_r128<W - 1>()(tmpL, i, b);
		callMeta_ShiftRightBitF2_511_r128<W - 1>()(tmpR, W - 1 - i, b);
		for (int j = 0; j < nW; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			/* !!! W <= nReg * regSize / 2 (=256) を仮定する !!! */
			if ( getBit32(arr32, i + (j * W) ) == 1) {

				callMeta_ShiftLeftBitF2_511_r128<(nW - 1)*W>()(tmp, j * W, tmpL);
				result.low ^= tmp;

				callMeta_ShiftRightBitF2_511_r128<(nW - 1)*W>()(tmp, (nW - j - 1)*W, tmpR);
				result.high ^= tmp;
			}
		}
	}


	result.clearUpDeg();
	result.reduction(c);

#endif
}

/*
c = a * x^m
*/
template<>
template<int M>
My_Force_Inline void F2_511_r128::mulMonomial(F2_BaseField& c, const F2_BaseField& a) {

//#if 0
	if (a.testZero()) {

		c.clearReg();
		return;
	}
//#endif

	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	result.low.copyReg(shiftLeftBit<M>(a));
	result.high.copyReg(shiftRightBit<511 - M>(a));

	//result.clearUpDeg();
	//result.low.clearUpDeg();

	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);
	result.low.xReg[3] &= mask128;

	result.reduction(c);
}

/*
(a_510x^510 + ... + a_1x^1 + a_0) = a_510x^1020 + ... + a_256x^512   <-- high
                                  +  a_255x^510 + ... +        a_0   <-- low
*/
template<>
My_Force_Inline void F2_511_r128::square(F2_BaseField& c, const F2_BaseField& a) {

//#if 0
	if (a.testZero()) {

		c.clearReg();
		return;
	}
//#endif

	int tmpN, tmpR;
	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	F2_511_r128 tmp;
	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];

	a.copyTo32(arr32);

#if 1

	uint32_t lowArr[16], highArr[16];
	for (int i = 0; i < 16; ++i) {
		lowArr[i] = 0;
		highArr[i] = 0;
	}

	for (int i = 0; i < 256; ++i) {

		//if (getBit32(arr32, i) == 1) lowArr[(2 * i) / 32] ^= 1 << ((2 * i) % 32);

		tmpN = (2 * i) / 32;
		tmpR = (2 * i) - (32 * tmpN);
		if (getBit32(arr32, i) == 1) lowArr[tmpN] ^= 1 << tmpR;
	}

	// high; index = 256 to 510
	for (int i = 0; i < 255; ++i) {

		//if (getBit32(arr32, i + 256) == 1) highArr[(2 * i + 1) / 32] ^= 1 << ((2 * i + 1) % 32); /* 1!! 511で区切っているため，highの0番目のビットは0（x^511の係数）から始まる !!!*/

		tmpN = (2 * i + 1) / 32;
		tmpR = (2 * i + 1) - (32 * tmpN);
		if (getBit32(arr32, i + 256) == 1) highArr[tmpN] ^= 1 << tmpR;
	}

	for (int i = 0; i < 4; ++i)	{
		result.low.xReg[i].set32(lowArr + 4 * i);
		result.high.xReg[i].set32(highArr + 4 * i);
	}


#else

	// low; index = 0 to 255
	for (int i = 0; i < 256; ++i) {

		if (getBit32(arr32, i) == 1) result.low.setBit1(2 * i);
	}

	// high; index = 256 to 510
	for (int i = 0; i < 255; ++i) {

		if (getBit32(arr32, i + 256) == 1) result.high.setBit1(2 * i + 1); /* 1!! 511で区切っているため，highの0番目のビットは0（x^511の係数）から始まる !!!*/
	}

#endif

	// no need to clearUpDeg
	result.reduction(c);
}

/* Comb multiplication with window method */

/*
precompute table for multiplication with window of width wSize

     coefficient | a0 a1 a2 a3 a4
index            |
---------------------------------
  0              |  0  0  0  0  0
  1              |  1  0  0  0  0
  2              |  0  1  0  0  0
  3              |  1  1  0  0  0
- - - - - - - - - - - - - - - - -
  4              |  0  0  1  0  0
  5              |  1  0  1  0  0
  6              |  0  1  1  0  0
  7              |  1  1  1  0  0
 -  -  -  -  -  -  -  -  -  -  -
  8              |  0  0  0  1  0
  9              |  1  0  0  1  0
 10              |  0  1  0  1  0
 11              |  1  1  0  1  0
 12              |  0  0  1  1  0
 13              |  1  0  1  1  0
 14              |  0  1  1  1  0
 15              |  1  1  1  1  0
  -   -   -   -   -   -   -   -
 16              |  0  0  0  0  1
 17              |  1  0  0  0  1
 18              |  0  1  0  0  1
 19              |  1  1  0  0  1
 20              |  0  0  1  0  1
 21              |  1  0  1  0  1
 22              |  0  1  1  0  1
 23              |  1  1  1  0  1
 24              |  0  0  0  1  1
 25              |  1  0  0  1  1
 26              |  0  1  0  1  1
 27              |  1  1  0  1  1
 28              |  0  0  1  1  1
 29              |  1  0  1  1  1
 30              |  0  1  1  1  1
 31              |  1  1  1  1  1
---------------------------------
 */


template<>
template<int wSize>
My_Force_Inline void F2_511_r128::precomputeTable(F2_BaseField *table, const F2_BaseField& a) {

	for (int i = 0; i < power2<wSize>::num; ++i) table[i].clear();
	table[1].copyReg(a);

	if (wSize > 1) {

		mulMonomial<1>(table[2], a); // a * x
		table[3] = table[1] ^ table[2];
	}

	if (wSize > 2) {

		mulMonomial<2>(table[4], a); // a * x^2
		table[5] = table[1] ^ table[4];
		table[6] = table[2] ^ table[4];
		table[7] = table[3] ^ table[4];
	}

	if (wSize > 3) {

		mulMonomial<3>(table[8], a); // a * x^3
		for (int i = 1; i < 8; ++i) table[8 + i] = table[i] ^ table[8];
	}

	if (wSize > 4) {

		mulMonomial<4>(table[16], a); // a * x^4
		for (int i = 1; i < 16; ++i) table[16 + i] = table[i] ^ table[16];
	}

	if (wSize > 5) {

		mulMonomial<5>(table[32], a); // a * x^5
		for (int i = 1; i < 32; ++i) table[32 + i] = table[i] ^ table[32];
	}

	if (wSize > 6) {

		mulMonomial<6>(table[64], a); // a * x^6
 		for (int i = 1; i < 64; ++i) table[64 + i] = table[i] ^ table[64];
	}

	if (wSize > 7) {

		mulMonomial<7>(table[128], a); // a * x^7
 		for (int i = 1; i < 128; ++i) table[128 + i] = table[i] ^ table[128];
	}

	if (wSize > 8) {

		mulMonomial<8>(table[256], a); // a * x^8
 		for (int i = 1; i < 256; ++i) table[256 + i] = table[i] ^ table[256];
	}
}

/* 最適化時inline展開するとコード生成にかなりの時間が掛かる */
template<>
template<int wSize>
//My_Force_Inline void F2_511_r128::mulCombL2RwithWindow(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {
void F2_511_r128::mulCombL2RwithWindow(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {

	if (a.testZero() || b.testZero()) {

		c.clearReg();
		return;
	}

	static const unsigned int nW = (511 + F2_511_r128::regLength - 1) / F2_511_r128::regLength;
	static const unsigned int n_wSize = F2_511_r128::regLength / wSize;
	static const unsigned int rem_wSize = F2_511_r128::regLength % wSize;
	//static const unsigned int rem_wSize = F2_511_r128::regLength - wSize * n_wSize;
	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);

	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];
	a.copyTo32(arr32);

	F2_511_r128 bTable[power2<wSize>::num];
	F2_511_r128 tmpL, tmpH;

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//F2_511_r128 bTable[power2Arr[wSize]];
	precomputeTable<wSize>(bTable, b);


	/*
	F2_BaseField_Doubleを二つのF2_511_r128型の値を持つものとは見做さず，low値はフルで512 bit使い，
	最後のreductionの前にshiftして整形する
	*/

	// remainder
	if (rem_wSize != 0) {
		for (int j = 0; j < nW; ++j) {

			//result.low ^= F2_511_r128::shiftLeftRegSize(bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_511_r128::regLength)], j);
			F2_511_r128::shiftLeftRegSize(tmpL, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_511_r128::regLength)], j);
			result.low ^= tmpL;

			//result.high ^= F2_511_r128::shiftRightRegSize(bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_511_r128::regLength)], nW - j);
			F2_511_r128::shiftRightRegSize(tmpH, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_511_r128::regLength)], nW - j);
			result.high ^= tmpH;
		}

		// result *= x^wSize (Left to Right Comb method)
		result.high.shiftLeftBit<wSize>();

		//result.high ^= F2_511_r128::shiftRightBit<512 - wSize>(result.low); /*上記理由から511でなく512*/
		F2_511_r128::shiftRightBit<512 - wSize>(tmpH, result.low);
		result.high ^= tmpH;

		result.low.shiftLeftBit<wSize>();
	}

	for (int i = n_wSize-1; i >= 0; --i) {
		for (int j = 0; j < nW; ++j) {

			//result.low ^= F2_511_r128::shiftLeftRegSize(bTable[index<wSize>(arr32, i*wSize+j*F2_511_r128::regLength)], j);
			F2_511_r128::shiftLeftRegSize(tmpL, bTable[index<wSize>(arr32, i*wSize + j*F2_511_r128::regLength)], j);
			result.low ^= tmpL;

			//result.high ^= F2_511_r128::shiftRightRegSize(bTable[index<wSize>(arr32, i*wSize+j*F2_511_r128::regLength)], nW - j);
			F2_511_r128::shiftRightRegSize(tmpH, bTable[index<wSize>(arr32, i*wSize + j*F2_511_r128::regLength)], nW - j);
			result.high ^= tmpH;
		}

		if (i != 0) {

			// result *= x^wSize (Left to Right Comb method)
			result.high.shiftLeftBit<wSize>();

			//result.high ^= F2_511_r128::shiftRightBit<512 - wSize>(result.low); /*上記理由から511でなく512*/
			F2_511_r128::shiftRightBit<512 - wSize>(tmpH, result.low);
			result.high ^= tmpH;

			result.low.shiftLeftBit<wSize>();
		}
	}

	/*
	F2_BaseField_Double<F2_511_r128>としての表現に合わせて整形
	*/
	result.high.shiftLeftBit1();

	//result.high ^= F2_511_r128::shiftRightBit<511>(result.low);
	F2_511_r128::shiftRightBit<511>(tmpH, result.low);
	result.high ^= tmpH;

	result.low.xReg[3] &= mask128; // clear coefficinets of upper degree

	//result.clearUpDeg();
	//result.low.clearUpDeg();

	result.reduction(c);
}

template<>
My_Force_Inline void F2_511_r128::mulLoadShift8(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {

//#if 0
	if (a.testZero() || b.testZero()) {

		c.clearReg();
		return;
	}
//#endif

	//static const int nW = (487 + F2_511_r128::regLength - 1) / F2_511_r128::regLength;
	//static const int nW = F2_511_r128::nR;

	F2_BaseField_Double<F2_511_r128> result;
	result.clear();

	//F2_511_r128 tmpL, tmpR;

	My_Align(16) uint32_t arr32[16];
	//__declspec(align(16)) uint32_t arr32[16];
	a.copyTo32(arr32);

	/*
   191  128 127        64 63    0
	0,...,0 b63,...,b1,b0 0,...,0
	*/
	uint8_t bArr8[64 * 3];

	for (int i = 0; i < 64; ++i) {
		bArr8[i] = 0;
		bArr8[i + 128] = 0;
	}

	F2_511_r128 bL, bH, tmpBF;
	bL = b;
	bH = b;

	for (int i = 0; i < 4; ++i) _mm_storeu_si128((__m128i*)(bArr8 + 64 + i * 16), b.xReg[i].x);

	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 64; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			if (getBit32(arr32, i + j * 8) == 1) {

				for (int k = 0; k < 4; ++k) tmpBF.xReg[k].x = _mm_loadu_si128((const __m128i*)(bArr8 + 64 - j + k * 16));
				result.low ^= tmpBF;
			}
		}

		bL.shiftLeftBit1();
		for (int rIdx = 0; rIdx < 4; ++rIdx) _mm_storeu_si128((__m128i*)(bArr8 + 64 + rIdx * 16), bL.xReg[rIdx].x);
	}

	/* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
	/* m=511のときはシフトする必要はない (bH) */
	for (int i = 0; i < 4; ++i) _mm_storeu_si128((__m128i*)(bArr8 + 64 + i * 16), bH.xReg[i].x);

	for (int i = 7; i >= 0; --i) {
		for (int j = 0; j < 64; ++j) {

			/* aは定められた次数より大きい項の係数が0であることを仮定する */
			if (getBit32(arr32, i + j * 8) == 1) {

				for (int k = 0; k < 4; ++k) tmpBF.xReg[k].x = _mm_loadu_si128((const __m128i*)(bArr8 + 64 + 64 - j - 1 + k * 16));
				result.high ^= tmpBF;
			}
		}

		bH.shiftRightBit1();
		for (int rIdx = 0; rIdx < 4; ++rIdx) _mm_storeu_si128((__m128i*)(bArr8 + 64 + rIdx * 16), bH.xReg[rIdx].x);
	}

	//result.clearUpDeg();
	//result.low.clearUpDeg();

	char2_128 mask128(0xffffffff, 0xffffffff, 0xffffffff, 0x7fffffff);
	result.low.xReg[3] &= mask128;
	result.reduction(c);
}
