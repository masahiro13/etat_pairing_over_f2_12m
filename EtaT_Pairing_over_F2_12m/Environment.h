#pragma once

#ifdef _MSC_VER
  #define My_Align(x) __declspec(align(x))
  #define My_Force_Inline __forceinline
#else
  #define My_Align(x) __attribute__((aligned(x)))
  //#define My_Force_Inline __attribute__((always_inline))
  #define My_Force_Inline inline
#endif
