#pragma once

#include "base_field.h"

/*

extension field: $\mathbb F_{2^{6m}}$の定義
\mathbb F_{2^{6m}} = \mathbb F_2[x]/(x^6+x^5+x^3+x^2+1)

*/
template<class T> class F2_ExtField6 {

public:

	static const int ext_degree = 6;
	static const int base_degree = T::ext_degree;
	static const int regLength = T::regLength;

	T xBF[6];



	F2_ExtField6() {}

#if 0
	F2_ExtField6(const T& a0, const T& a1, const T& a2, const T& a3, const T& a4, const T& a5)
		: xBF[0](a0)
		, xBF[1](a1)
		, xBF[2](a2)
		, xBF[3](a3)
		, xBF[4](a4)
		, xBF[5](a5)
	{}

	F2_ExtField6(const T *a)
		: xBF[0](a[0])
		, xBF[1](a[1])
		, xBF[2](a[2])
		, xBF[3](a[3])
		, xBF[4](a[4])
		, xBF[5](a[5])
	{}
#endif

	void clear() {

		xBF[0].clear();
		xBF[1].clear();
		xBF[2].clear();
		xBF[3].clear();
		xBF[4].clear();
		xBF[5].clear();
	}

	void clearUpDeg() {

		xBF[0].clearUpDeg();
		xBF[1].clearUpDeg();
		xBF[2].clearUpDeg();
		xBF[3].clearUpDeg();
		xBF[4].clearUpDeg();
		xBF[5].clearUpDeg();
	}

	void setRandom() {

		xBF[0].setRandom();
		xBF[1].setRandom();
		xBF[2].setRandom();
		xBF[3].setRandom();
		xBF[4].setRandom();
		xBF[5].setRandom();
	}

	void copyReg(const F2_ExtField6& a) {

		this->xBF[0].copyReg(a.xBF[0]);
		this->xBF[1].copyReg(a.xBF[1]);
		this->xBF[2].copyReg(a.xBF[2]);
		this->xBF[3].copyReg(a.xBF[3]);
		this->xBF[4].copyReg(a.xBF[4]);
		this->xBF[5].copyReg(a.xBF[5]);
	}

	void plusOne() {

		xBF[0].plusOne();
	}

	friend F2_ExtField6 operator+(const F2_ExtField6& a, const F2_ExtField6& b) {

		F2_ExtField6 c;

		c.xBF[0] = a.xBF[0] ^ b.xBF[0];
		c.xBF[1] = a.xBF[1] ^ b.xBF[1];
		c.xBF[2] = a.xBF[2] ^ b.xBF[2];
		c.xBF[3] = a.xBF[3] ^ b.xBF[3];
		c.xBF[4] = a.xBF[4] ^ b.xBF[4];
		c.xBF[5] = a.xBF[5] ^ b.xBF[5];
		return c;
	}

	friend F2_ExtField6 operator-(const F2_ExtField6& a, const F2_ExtField6& b) {

		F2_ExtField6 c;

		c.xBF[0] = a.xBF[0] ^ b.xBF[0];
		c.xBF[1] = a.xBF[1] ^ b.xBF[1];
		c.xBF[2] = a.xBF[2] ^ b.xBF[2];
		c.xBF[3] = a.xBF[3] ^ b.xBF[3];
		c.xBF[4] = a.xBF[4] ^ b.xBF[4];
		c.xBF[5] = a.xBF[5] ^ b.xBF[5];
		return c;
	}

	friend bool operator==(const F2_ExtField6& a, const F2_ExtField6& b) {

		bool result = true;

		result &= (a.xBF[0] == b.xBF[0]);
		result &= (a.xBF[1] == b.xBF[1]);
		result &= (a.xBF[2] == b.xBF[2]);
		result &= (a.xBF[3] == b.xBF[3]);
		result &= (a.xBF[4] == b.xBF[4]);
		result &= (a.xBF[5] == b.xBF[5]);
		return result;
	}

	F2_ExtField6 operator+=(const F2_ExtField6& b) {

		xBF[0] ^= b.xBF[0];
		xBF[1] ^= b.xBF[1];
		xBF[2] ^= b.xBF[2];
		xBF[3] ^= b.xBF[3];
		xBF[4] ^= b.xBF[4];
		xBF[5] ^= b.xBF[5];
		return *this;
	}

	F2_ExtField6 operator-=(const F2_ExtField6& b) {

		xBF[0] ^= b.xBF[0];
		xBF[1] ^= b.xBF[1];
		xBF[2] ^= b.xBF[2];
		xBF[3] ^= b.xBF[3];
		xBF[4] ^= b.xBF[4];
		xBF[5] ^= b.xBF[5];
		return *this;
	}

	/*

	reduction with defining polynomial w^6 + w^5 + w^3 + w^2 + 1

	a_10w^10 + a_9w^9 + a_8w^8 + a_7w^7 + a_6w^6 + a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0
	= (a_7 + a_6 + a_5)w^5 + (a_7 + a_4)w^4 + (a_10 + a_6 + a_3)w^3 + (a_10 + a_9 + a_7 + a_6 + a_2)w^2 + (a_9 + a_8 + a_7 + a_1)w + a_8 + a_7 + a_6 + a_0

	*/
	void reduction(const F2_ExtField_Double<F2_ExtField6>& c) {

		T a67, tmp;

		// a6 + a7
		a67.copyReg(c.high.xBF[0] + c.high.xBF[1]);

		// (a7 + a6 + a5)w^5
		this->xBF[5] = a67 + c.low.xBF[5];

		// (a7 + a4)w^4
		this->xBF[4] = c.high.xBF[1] + c.low.xBF[4];

		// (a10 + a6 + a3)w^3
		tmp = c.high.xBF[4] + c.high.xBF[0]; 
		this->xBF[3] = tmp + c.low.xBF[3];

		// (a_10 + a_9 + a_7 + a_6 + a_2)w^2
		tmp = a67 + c.low.xBF[2];
		tmp += c.high.xBF[3];
		this->xBF[2] = tmp + c.high.xBF[4];

		// (a_9 + a_8 + a_7 + a_1)w
		tmp = c.low.xBF[1] + c.high.xBF[1];
		tmp += c.high.xBF[2];
		this->xBF[1] = tmp + c.high.xBF[3];

		// a_8 + a_7 + a_6 + a_0
		tmp = a67 + c.low.xBF[0];
		this->xBF[0] = tmp + c.high.xBF[2];
	}

	// Karatsuba 6
	static void mulShAdd(F2_ExtField6& c, const F2_ExtField6& a, const F2_ExtField6& b) {

		T v[6], vST[15];
		F2_ExtField_Double<F2_ExtField6> result;

		// v[i] = a[i]b[i], not use v[0], v[5]
		for (int i = 1; i < 5; ++i) T::mulShAdd(v[i], a.xBF[i], b.xBF[i]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulShAdd(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulShAdd(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulShAdd(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
		T::mulShAdd(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
		T::mulShAdd(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
		T::mulShAdd(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
		T::mulShAdd(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
		T::mulShAdd(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
		T::mulShAdd(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
		T::mulShAdd(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
		T::mulShAdd(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
		T::mulShAdd(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
		T::mulShAdd(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
		T::mulShAdd(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
		T::mulShAdd(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



		// res[0] = a[0]b[0]
		T::mulShAdd(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[10] = a[5]b[5]
		T::mulShAdd(result.high.xBF[4], a.xBF[5], b.xBF[5]);

		// res[1]
		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v[1];

		// res[2]
		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= v[2];
		result.low.xBF[2] += v[1];

		// res[3]
		result.low.xBF[3] = vST[2] + vST[5];
		result.low.xBF[3] -= result.low.xBF[0];
		result.low.xBF[3] -= v[3];
		result.low.xBF[3] -= v[1];
		result.low.xBF[3] -= v[2];

		// res[4]
		result.low.xBF[4] = vST[3] + vST[6];
		result.low.xBF[4] -= result.low.xBF[0];
		result.low.xBF[4] -= v[4];
		result.low.xBF[4] -= v[1];
		result.low.xBF[4] -= v[3];
		result.low.xBF[4] += v[2];

		// res[5]
		result.low.xBF[5] = vST[4] + vST[7];
		result.low.xBF[5] += vST[9];
		result.low.xBF[5] -= result.low.xBF[0];
		result.low.xBF[5] -= result.high.xBF[4];
		result.low.xBF[5] -= v[1];
		result.low.xBF[5] -= v[4];
		result.low.xBF[5] -= v[2];
		result.low.xBF[5] -= v[3];

		// res[6]
		result.high.xBF[0] = vST[8] + vST[10];
		result.high.xBF[0] -= v[1];
		result.high.xBF[0] -= result.high.xBF[4];
		result.high.xBF[0] -= v[2];
		result.high.xBF[0] -= v[4];
		result.high.xBF[0] += v[3];

		// res[7]
		result.high.xBF[1] = vST[11] + vST[12];
		result.high.xBF[1] -= v[2];
		result.high.xBF[1] -= result.high.xBF[4];
		result.high.xBF[1] -= v[3];
		result.high.xBF[1] -= v[4];

		// res[8]
		result.high.xBF[2] = vST[13] - v[3];
		result.high.xBF[2] -= result.high.xBF[4];
		result.high.xBF[2] += v[4];

		// res[9]
		result.high.xBF[3] = vST[14] - v[4];
		result.high.xBF[3] -= result.high.xBF[4];

		c.reduction(result);
	}

	static void mulCombR2L_RegSize(F2_ExtField6& c, const F2_ExtField6& a, const F2_ExtField6& b) {

		T v[6], vST[15];
		F2_ExtField_Double<F2_ExtField6> result;

		// v[i] = a[i]b[i], not use v[0], v[5]
		for (int i = 1; i < 5; ++i) T::mulCombR2L_RegSize(v[i], a.xBF[i], b.xBF[i]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulCombR2L_RegSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulCombR2L_RegSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulCombR2L_RegSize(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
		T::mulCombR2L_RegSize(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
		T::mulCombR2L_RegSize(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
		T::mulCombR2L_RegSize(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
		T::mulCombR2L_RegSize(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
		T::mulCombR2L_RegSize(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
		T::mulCombR2L_RegSize(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
		T::mulCombR2L_RegSize(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
		T::mulCombR2L_RegSize(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
		T::mulCombR2L_RegSize(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
		T::mulCombR2L_RegSize(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
		T::mulCombR2L_RegSize(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
		T::mulCombR2L_RegSize(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



		// res[0] = a[0]b[0]
		T::mulCombR2L_RegSize(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[10] = a[5]b[5]
		T::mulCombR2L_RegSize(result.high.xBF[4], a.xBF[5], b.xBF[5]);

		// res[1]
		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v[1];

		// res[2]
		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= v[2];
		result.low.xBF[2] += v[1];

		// res[3]
		result.low.xBF[3] = vST[2] + vST[5];
		result.low.xBF[3] -= result.low.xBF[0];
		result.low.xBF[3] -= v[3];
		result.low.xBF[3] -= v[1];
		result.low.xBF[3] -= v[2];

		// res[4]
		result.low.xBF[4] = vST[3] + vST[6];
		result.low.xBF[4] -= result.low.xBF[0];
		result.low.xBF[4] -= v[4];
		result.low.xBF[4] -= v[1];
		result.low.xBF[4] -= v[3];
		result.low.xBF[4] += v[2];

		// res[5]
		result.low.xBF[5] = vST[4] + vST[7];
		result.low.xBF[5] += vST[9];
		result.low.xBF[5] -= result.low.xBF[0];
		result.low.xBF[5] -= result.high.xBF[4];
		result.low.xBF[5] -= v[1];
		result.low.xBF[5] -= v[4];
		result.low.xBF[5] -= v[2];
		result.low.xBF[5] -= v[3];

		// res[6]
		result.high.xBF[0] = vST[8] + vST[10];
		result.high.xBF[0] -= v[1];
		result.high.xBF[0] -= result.high.xBF[4];
		result.high.xBF[0] -= v[2];
		result.high.xBF[0] -= v[4];
		result.high.xBF[0] += v[3];

		// res[7]
		result.high.xBF[1] = vST[11] + vST[12];
		result.high.xBF[1] -= v[2];
		result.high.xBF[1] -= result.high.xBF[4];
		result.high.xBF[1] -= v[3];
		result.high.xBF[1] -= v[4];

		// res[8]
		result.high.xBF[2] = vST[13] - v[3];
		result.high.xBF[2] -= result.high.xBF[4];
		result.high.xBF[2] += v[4];

		// res[9]
		result.high.xBF[3] = vST[14] - v[4];
		result.high.xBF[3] -= result.high.xBF[4];

		c.reduction(result);
	}

	static void mulLoadShift8(F2_ExtField6& c, const F2_ExtField6& a, const F2_ExtField6& b) {

		T v[6], vST[15];
		F2_ExtField_Double<F2_ExtField6> result;

		// v[i] = a[i]b[i], not use v[0], v[5]
		for (int i = 1; i < 5; ++i) T::mulLoadShift8(v[i], a.xBF[i], b.xBF[i]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::mulLoadShift8(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulLoadShift8(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::mulLoadShift8(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
		T::mulLoadShift8(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
		T::mulLoadShift8(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
		T::mulLoadShift8(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
		T::mulLoadShift8(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
		T::mulLoadShift8(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
		T::mulLoadShift8(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
		T::mulLoadShift8(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
		T::mulLoadShift8(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
		T::mulLoadShift8(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
		T::mulLoadShift8(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
		T::mulLoadShift8(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
		T::mulLoadShift8(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



		// res[0] = a[0]b[0]
		T::mulLoadShift8(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[10] = a[5]b[5]
		T::mulLoadShift8(result.high.xBF[4], a.xBF[5], b.xBF[5]);

		// res[1]
		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v[1];

		// res[2]
		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= v[2];
		result.low.xBF[2] += v[1];

		// res[3]
		result.low.xBF[3] = vST[2] + vST[5];
		result.low.xBF[3] -= result.low.xBF[0];
		result.low.xBF[3] -= v[3];
		result.low.xBF[3] -= v[1];
		result.low.xBF[3] -= v[2];

		// res[4]
		result.low.xBF[4] = vST[3] + vST[6];
		result.low.xBF[4] -= result.low.xBF[0];
		result.low.xBF[4] -= v[4];
		result.low.xBF[4] -= v[1];
		result.low.xBF[4] -= v[3];
		result.low.xBF[4] += v[2];

		// res[5]
		result.low.xBF[5] = vST[4] + vST[7];
		result.low.xBF[5] += vST[9];
		result.low.xBF[5] -= result.low.xBF[0];
		result.low.xBF[5] -= result.high.xBF[4];
		result.low.xBF[5] -= v[1];
		result.low.xBF[5] -= v[4];
		result.low.xBF[5] -= v[2];
		result.low.xBF[5] -= v[3];

		// res[6]
		result.high.xBF[0] = vST[8] + vST[10];
		result.high.xBF[0] -= v[1];
		result.high.xBF[0] -= result.high.xBF[4];
		result.high.xBF[0] -= v[2];
		result.high.xBF[0] -= v[4];
		result.high.xBF[0] += v[3];

		// res[7]
		result.high.xBF[1] = vST[11] + vST[12];
		result.high.xBF[1] -= v[2];
		result.high.xBF[1] -= result.high.xBF[4];
		result.high.xBF[1] -= v[3];
		result.high.xBF[1] -= v[4];

		// res[8]
		result.high.xBF[2] = vST[13] - v[3];
		result.high.xBF[2] -= result.high.xBF[4];
		result.high.xBF[2] += v[4];

		// res[9]
		result.high.xBF[3] = vST[14] - v[4];
		result.high.xBF[3] -= result.high.xBF[4];

		c.reduction(result);
	}

	template<int wSize>
	static void mulCombL2RwithWindow(F2_ExtField6& c, const F2_ExtField6& a, const F2_ExtField6& b) {

		T v[6], vST[15];
		F2_ExtField_Double<F2_ExtField6> result;

		// v[i] = a[i]b[i], not use v[0], v[5]
		for (int i = 1; i < 5; ++i) T::template mulCombL2RwithWindow<wSize>(v[i], a.xBF[i], b.xBF[i]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
		T::template mulCombL2RwithWindow<wSize>(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
		T::template mulCombL2RwithWindow<wSize>(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
		T::template mulCombL2RwithWindow<wSize>(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
		T::template mulCombL2RwithWindow<wSize>(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
		T::template mulCombL2RwithWindow<wSize>(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
		T::template mulCombL2RwithWindow<wSize>(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
		T::template mulCombL2RwithWindow<wSize>(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
		T::template mulCombL2RwithWindow<wSize>(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



		// res[0] = a[0]b[0]
		T::template mulCombL2RwithWindow<wSize>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[10] = a[5]b[5]
		T::template mulCombL2RwithWindow<wSize>(result.high.xBF[4], a.xBF[5], b.xBF[5]);

		// res[1]
		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v[1];

		// res[2]
		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= v[2];
		result.low.xBF[2] += v[1];

		// res[3]
		result.low.xBF[3] = vST[2] + vST[5];
		result.low.xBF[3] -= result.low.xBF[0];
		result.low.xBF[3] -= v[3];
		result.low.xBF[3] -= v[1];
		result.low.xBF[3] -= v[2];

		// res[4]
		result.low.xBF[4] = vST[3] + vST[6];
		result.low.xBF[4] -= result.low.xBF[0];
		result.low.xBF[4] -= v[4];
		result.low.xBF[4] -= v[1];
		result.low.xBF[4] -= v[3];
		result.low.xBF[4] += v[2];

		// res[5]
		result.low.xBF[5] = vST[4] + vST[7];
		result.low.xBF[5] += vST[9];
		result.low.xBF[5] -= result.low.xBF[0];
		result.low.xBF[5] -= result.high.xBF[4];
		result.low.xBF[5] -= v[1];
		result.low.xBF[5] -= v[4];
		result.low.xBF[5] -= v[2];
		result.low.xBF[5] -= v[3];

		// res[6]
		result.high.xBF[0] = vST[8] + vST[10];
		result.high.xBF[0] -= v[1];
		result.high.xBF[0] -= result.high.xBF[4];
		result.high.xBF[0] -= v[2];
		result.high.xBF[0] -= v[4];
		result.high.xBF[0] += v[3];

		// res[7]
		result.high.xBF[1] = vST[11] + vST[12];
		result.high.xBF[1] -= v[2];
		result.high.xBF[1] -= result.high.xBF[4];
		result.high.xBF[1] -= v[3];
		result.high.xBF[1] -= v[4];

		// res[8]
		result.high.xBF[2] = vST[13] - v[3];
		result.high.xBF[2] -= result.high.xBF[4];
		result.high.xBF[2] += v[4];

		// res[9]
		result.high.xBF[3] = vST[14] - v[4];
		result.high.xBF[3] -= result.high.xBF[4];

		c.reduction(result);
	}

	template<int W>
	static void mulCombR2L(F2_ExtField6& c, const F2_ExtField6& a, const F2_ExtField6& b) {

		T v[6], vST[15];
		F2_ExtField_Double<F2_ExtField6> result;

		// v[i] = a[i]b[i], not use v[0], v[5]
		for (int i = 1; i < 5; ++i) T::template mulCombR2L<W>(v[i], a.xBF[i], b.xBF[i]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		T::template mulCombR2L<W>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::template mulCombR2L<W>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		T::template mulCombR2L<W>(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
		T::template mulCombR2L<W>(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
		T::template mulCombR2L<W>(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
		T::template mulCombR2L<W>(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
		T::template mulCombR2L<W>(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
		T::template mulCombR2L<W>(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
		T::template mulCombR2L<W>(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
		T::template mulCombR2L<W>(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
		T::template mulCombR2L<W>(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
		T::template mulCombR2L<W>(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
		T::template mulCombR2L<W>(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
		T::template mulCombR2L<W>(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
		T::template mulCombR2L<W>(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



		// res[0] = a[0]b[0]
		T::template mulCombR2L<W>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

		// res[10] = a[5]b[5]
		T::template mulCombR2L<W>(result.high.xBF[4], a.xBF[5], b.xBF[5]);

		// res[1]
		result.low.xBF[1] = vST[0] - result.low.xBF[0];
		result.low.xBF[1] -= v[1];

		// res[2]
		result.low.xBF[2] = vST[1] - result.low.xBF[0];
		result.low.xBF[2] -= v[2];
		result.low.xBF[2] += v[1];

		// res[3]
		result.low.xBF[3] = vST[2] + vST[5];
		result.low.xBF[3] -= result.low.xBF[0];
		result.low.xBF[3] -= v[3];
		result.low.xBF[3] -= v[1];
		result.low.xBF[3] -= v[2];

		// res[4]
		result.low.xBF[4] = vST[3] + vST[6];
		result.low.xBF[4] -= result.low.xBF[0];
		result.low.xBF[4] -= v[4];
		result.low.xBF[4] -= v[1];
		result.low.xBF[4] -= v[3];
		result.low.xBF[4] += v[2];

		// res[5]
		result.low.xBF[5] = vST[4] + vST[7];
		result.low.xBF[5] += vST[9];
		result.low.xBF[5] -= result.low.xBF[0];
		result.low.xBF[5] -= result.high.xBF[4];
		result.low.xBF[5] -= v[1];
		result.low.xBF[5] -= v[4];
		result.low.xBF[5] -= v[2];
		result.low.xBF[5] -= v[3];

		// res[6]
		result.high.xBF[0] = vST[8] + vST[10];
		result.high.xBF[0] -= v[1];
		result.high.xBF[0] -= result.high.xBF[4];
		result.high.xBF[0] -= v[2];
		result.high.xBF[0] -= v[4];
		result.high.xBF[0] += v[3];

		// res[7]
		result.high.xBF[1] = vST[11] + vST[12];
		result.high.xBF[1] -= v[2];
		result.high.xBF[1] -= result.high.xBF[4];
		result.high.xBF[1] -= v[3];
		result.high.xBF[1] -= v[4];

		// res[8]
		result.high.xBF[2] = vST[13] - v[3];
		result.high.xBF[2] -= result.high.xBF[4];
		result.high.xBF[2] += v[4];

		// res[9]
		result.high.xBF[3] = vST[14] - v[4];
		result.high.xBF[3] -= result.high.xBF[4];

		c.reduction(result);
	}

	/*

	(a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0)^2 = a_5^2w^10 + a_4^2w^8 + a_3^2w^6 + a_2^2w^4 + a_1^2w^1 + a_0^2
	                                                   = (a_3^2)w^5 + (a_2^2)w^4 + (a_5^2 + a_3^2)w^3 + (a_5^2 + a_3^2 + a_1^2)w^2 + (a_4^2)w + a_4^2 + a_3^2 + a_0^2

	*/
	static void square(F2_ExtField6& c, const F2_ExtField6& a) {

		// (fix) c_5 = a_3^2
		T::square(c.xBF[5], a.xBF[3]);

		// (fix) c_4 = a_2^2
		T::square(c.xBF[4], a.xBF[2]);

		// c_3 = a_5^2
		T::square(c.xBF[3], a.xBF[5]);

		// (fix) c_3 = a_5^2 + a_3^2
		c.xBF[3] += c.xBF[5];

		// c_0 = a_1^2
		T::square(c.xBF[0], a.xBF[1]);

		// (fix) c_2 = a_5^2 + a_3^2 + a_1^2
		c.xBF[2] = c.xBF[3] + c.xBF[0];

		// (fix) c_1 = a_4^2
		T::square(c.xBF[1], a.xBF[4]);

		// c_0 = a_0^2
		T::square(c.xBF[0], a.xBF[0]);

		// c_0 = a_4^2 + a_3^2 + a_0^2
		c.xBF[0] += c.xBF[1];
		c.xBF[0] += c.xBF[5];
	}

	/*

	(a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0) * (w^5 + w^3)
	= (a_0 + a_1 + a_3 + a_4)w^5 + (a_1 + a_2 + a_4)w^4 + (a_0 + a_1 + a_3 + a_5)w^3 + (a_1 + a_2 + a_3 + a_5)w^2 + (a_2 + a_3 + a_5)w + a_1 + a_2 + a_4 + a_5

	*/
	static void mulConstTerm(F2_ExtField6& c, const F2_ExtField6& a) {

		T tmp;

		// tmp = a_0 + a_1 + a_3
		tmp = a.xBF[0] + a.xBF[1];
		tmp += a.xBF[3];

		c.xBF[5] = tmp + a.xBF[4];
		c.xBF[3] = tmp + a.xBF[5];

		// tmp = a_1 + a_2 + a_4
		tmp = a.xBF[1] + a.xBF[2];
		tmp += a.xBF[4];

		c.xBF[0] = tmp + a.xBF[5];
		c.xBF[4] = tmp;

		// tmp = a_2 + a_3 + a_5
		tmp = a.xBF[2] + a.xBF[3];
		tmp += a.xBF[5];

		c.xBF[2] = tmp + a.xBF[1];
		c.xBF[1] = tmp;
	}
};



/*************************************************************************************/
/*************************************************************************************/

/*

extension field: $\mathbb F_{2^{12m}}$の定義
\mathbb F_{2^{12m}} = \mathbb F_{2^{12m}}[y]/(y^2+y+w^5+w^3), w^6+w^5+w^3+w^2+1=0

*/
template<class T> class F2_ExtField6_2 {

public:

	static const int ext_degree = 12;
	static const int base_degree = T::ext_degree;
	static const int regLength = T::regLength;
	static const int nFull = T::nFull;
	static const int stride32 = T::stride32;

	F2_ExtField6<T> xF6[2];



	F2_ExtField6_2() {}

#if 0
	F2_ExtField6_2(const F2_ExtField6<T>& a0, const F2_ExtField6<T>& a1)
		: xF6[0](a0)
		, xF6[1](a1)
	{}

	F2_ExtField6_2(const F2_ExtField6<T> *a)
		: xF6[0](a[0])
		, xF6[1](a[1])
	{}
#endif

	void clear() {

		xF6[0].clear();
		xF6[1].clear();
	}

	void setRandom() {

		xF6[0].setRandom();
		xF6[1].setRandom();
	}

	void copyReg(const F2_ExtField6_2& a) {

		this->xF6[0].copyReg(a.xF6[0]);
		this->xF6[1].copyReg(a.xF6[1]);
	}

	void plusOne() {

		xF6[0].plusOne();
	}

	friend F2_ExtField6_2 operator+(const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField6_2 c;

		c.xF6[0] = a.xF6[0] + b.xF6[0];
		c.xF6[1] = a.xF6[1] + b.xF6[1];
		return c;
	}

	friend bool operator==(const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		return (a.xF6[0] == b.xF6[0]) && (a.xF6[1] == b.xF6[1]);
	}

	F2_ExtField6_2 operator+=(const F2_ExtField6_2& b) {

		xF6[0] ^= b.xF6[0];
		xF6[1] ^= b.xF6[1];
		return *this;
	}

	/*

	reduction with defining polynomial s^2 + s + w^5 + w^3

	a_2s^2 + a_1s + a_0
	= (a_2 + a_1)s + a_2(w^5 + w^3) + a_0,

	*/
	void reduction(const F2_ExtField_Double<F2_ExtField6_2>& c) {

		F2_ExtField6<T> tmp;
		F2_ExtField6<T>::mulConstTerm(tmp, c.high.xF6[0]);
		this->xF6[0].copyReg(tmp + c.low.xF6[0]);

		this->xF6[1].copyReg(c.high.xF6[0] + c.low.xF6[1]);
	}

	// Karatsuba 2
	static void mulShAdd(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField_Double<F2_ExtField6_2> result;

		F2_ExtField6<T>::mulShAdd(result.low.xF6[0], a.xF6[0], b.xF6[0]);
		F2_ExtField6<T>::mulShAdd(result.high.xF6[0], a.xF6[1], b.xF6[1]);

		F2_ExtField6<T>::mulShAdd(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
		result.low.xF6[1] -= result.low.xF6[0];
		result.low.xF6[1] -= result.high.xF6[0];

		c.reduction(result);
	}

	static void mulCombR2L_RegSize(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField_Double<F2_ExtField6_2> result;

		F2_ExtField6<T>::mulCombR2L_RegSize(result.low.xF6[0], a.xF6[0], b.xF6[0]);
		F2_ExtField6<T>::mulCombR2L_RegSize(result.high.xF6[0], a.xF6[1], b.xF6[1]);

		F2_ExtField6<T>::mulCombR2L_RegSize(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
		result.low.xF6[1] -= result.low.xF6[0];
		result.low.xF6[1] -= result.high.xF6[0];

		c.reduction(result);
	}

	static void mulLoadShift8(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField_Double<F2_ExtField6_2> result;

		F2_ExtField6<T>::mulLoadShift8(result.low.xF6[0], a.xF6[0], b.xF6[0]);
		F2_ExtField6<T>::mulLoadShift8(result.high.xF6[0], a.xF6[1], b.xF6[1]);

		F2_ExtField6<T>::mulLoadShift8(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
		result.low.xF6[1] -= result.low.xF6[0];
		result.low.xF6[1] -= result.high.xF6[0];

		c.reduction(result);
	}

	template<int wSize>
	static void mulCombL2RwithWindow(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField_Double<F2_ExtField6_2> result;

		F2_ExtField6<T>::template mulCombL2RwithWindow<wSize>(result.low.xF6[0], a.xF6[0], b.xF6[0]);
		F2_ExtField6<T>::template mulCombL2RwithWindow<wSize>(result.high.xF6[0], a.xF6[1], b.xF6[1]);

		F2_ExtField6<T>::template mulCombL2RwithWindow<wSize>(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
		result.low.xF6[1] -= result.low.xF6[0];
		result.low.xF6[1] -= result.high.xF6[0];

		c.reduction(result);
	}

	template<int W>
	static void mulCombR2L(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6_2& b) {

		F2_ExtField_Double<F2_ExtField6_2> result;

		F2_ExtField6<T>::template mulCombR2L<W>(result.low.xF6[0], a.xF6[0], b.xF6[0]);
		F2_ExtField6<T>::template mulCombR2L<W>(result.high.xF6[0], a.xF6[1], b.xF6[1]);

		F2_ExtField6<T>::template mulCombR2L<W>(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
		result.low.xF6[1] -= result.low.xF6[0];
		result.low.xF6[1] -= result.high.xF6[0];

		c.reduction(result);
	}

	/*

	(a_1s + a_0)^2
	= a_1^2s^2 + a_0^2
	= (a_1^2)s + a_1^2(w^5 + w^3) + a_0^2,

	*/
	static void square(F2_ExtField6_2& c, const F2_ExtField6_2& a) {

		F2_ExtField6<T> a0sq;

		// c_1 = a_1^2
		F2_ExtField6<T>::square(c.xF6[1], a.xF6[1]);

		F2_ExtField6<T>::square(a0sq, a.xF6[0]);

		// c_0 = a_1^2(w^5 + w^3)
		F2_ExtField6<T>::mulConstTerm(c.xF6[0], c.xF6[1]);
		// c_0 = a_1^2(w^5 + w^3) + a_0^2
		c.xF6[0] += a0sq;
	}

	// \alpha\beta
	/*

	!! 乗算はregSizeのComb methodで行う !!
	(a0,a1,a2,a3,a4,a5) = a0 + a1w+ a2w^2 + a3w^3 + a4w^4 + a5w^5とする

	a=(a0,a1,a2,0,a4,0, (1,0,0,0,0,0)) = (f,1)
	b=(b0,b1,b2,0,b4,0, (1,0,0,0,0,0)) = (g,1)
	==> ab = (fg,f+g,1) = (fg + w^5 + w^3, f + g + 1)
           = (f + g + 1)s + fg + w^5 + w^3

	fg = (a0,a1,a2,0,a4,0)(b0,b1,b2,0,b4,0)
	   = (a0,a1,a2)(b0,b1,b2) + a4w^4(b0,b1,b2) + b4w^4(a0,a1,a2) + a4b4w^8
	   = (a0,a1,a2)(b0,b1,b2) + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4

	(a0,a1,a2)(b0,b1,b2)を3次のKaratsuba methodを用いて計算する

	*/
	static void alpha_beta(F2_ExtField6_2& c, const F2_ExtField6<T>& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);

		// f + g
		c.xF6[1] = a + b;
		// (fix) c_1 = f + g + 1
		c.xF6[1].xBF[0].xReg[0] ^= One128;

		// c_0 := (a0,a1,a2)(b0,b1,b2)
		// Karatsuba 3
		T v1, vST[3];

		T::mulCombR2L_RegSize(c.xF6[0].xBF[0], a.xBF[0], b.xBF[0]);
		T::mulCombR2L_RegSize(v1, a.xBF[1], b.xBF[1]);
		T::mulCombR2L_RegSize(c.xF6[0].xBF[4], a.xBF[2], b.xBF[2]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		// c_0_0
		T::mulCombR2L_RegSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulCombR2L_RegSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		// c_0_4
		T::mulCombR2L_RegSize(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		// c_0_1
		c.xF6[0].xBF[1] = vST[0] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[1] -= v1;

		// c_0_2
		c.xF6[0].xBF[2] = vST[1] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[2] -= c.xF6[0].xBF[4];
		c.xF6[0].xBF[2] += v1;

		// c_0_3
		c.xF6[0].xBF[3] = vST[2] - v1;
		c.xF6[0].xBF[3] -= c.xF6[0].xBF[4];

		//(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
		// (fix) v1 = a4b4
		T::mulCombR2L_RegSize(v1, a.xBF[4], b.xBF[4]);
		// (fix) vST[0] = a2b4 + a4b2
		T::mulCombR2L_RegSize(vST[0], a.xBF[2], b.xBF[4]);
		T::mulCombR2L_RegSize(vST[1], a.xBF[4], b.xBF[2]);
		vST[0] += vST[1];

		// c_0_5 = a1b4（初代入）
		T::mulCombR2L_RegSize(c.xF6[0].xBF[5], a.xBF[1], b.xBF[4]);
		T::mulCombR2L_RegSize(vST[1], a.xBF[4], b.xBF[1]);
		// c_0_5 = a1b4 + a4b1
		c.xF6[0].xBF[5] += vST[1];
		// (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
		c.xF6[0].xBF[5] += vST[0];

		// (fix) c_0_3 += a2b4 + a4b2
		c.xF6[0].xBF[3] += vST[0];

		// (fix) c_0_2 += a2b4 + a4b2
		c.xF6[0].xBF[2] += vST[0];

		// (fix) c_0_1 += a4b4
		c.xF6[0].xBF[1] += v1;

		// c_0_0 += a2b4 + a4b2
		c.xF6[0].xBF[0] += vST[0];
		// (fix) c_0_0 += a4b4
		c.xF6[0].xBF[0] += v1;

		T::mulCombR2L_RegSize(vST[1], a.xBF[0], b.xBF[4]);
		// c_0_4 += a0b4
		c.xF6[0].xBF[4] += vST[1];

		T::mulCombR2L_RegSize(vST[1], a.xBF[4], b.xBF[0]);
		// (fix) c_0_4 += a4b0
		c.xF6[0].xBF[4] += vST[1];

		// c_0 + w^5 + w^3
		c.xF6[0].xBF[3].xReg[0] ^= One128;
		c.xF6[0].xBF[5].xReg[0] ^= One128;
	}

	static void alpha_betaLoadShift8(F2_ExtField6_2& c, const F2_ExtField6<T>& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);

		// f + g
		c.xF6[1] = a + b;
		// (fix) c_1 = f + g + 1
		c.xF6[1].xBF[0].xReg[0] ^= One128;

		// c_0 := (a0,a1,a2)(b0,b1,b2)
		// Karatsuba 3
		T v1, vST[3];

		T::mulLoadShift8(c.xF6[0].xBF[0], a.xBF[0], b.xBF[0]);
		T::mulLoadShift8(v1, a.xBF[1], b.xBF[1]);
		T::mulLoadShift8(c.xF6[0].xBF[4], a.xBF[2], b.xBF[2]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		// c_0_0
		T::mulLoadShift8(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::mulLoadShift8(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		// c_0_4
		T::mulLoadShift8(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		// c_0_1
		c.xF6[0].xBF[1] = vST[0] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[1] -= v1;

		// c_0_2
		c.xF6[0].xBF[2] = vST[1] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[2] -= c.xF6[0].xBF[4];
		c.xF6[0].xBF[2] += v1;

		// c_0_3
		c.xF6[0].xBF[3] = vST[2] - v1;
		c.xF6[0].xBF[3] -= c.xF6[0].xBF[4];

		//(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
		// (fix) v1 = a4b4
		T::mulLoadShift8(v1, a.xBF[4], b.xBF[4]);
		// (fix) vST[0] = a2b4 + a4b2
		T::mulLoadShift8(vST[0], a.xBF[2], b.xBF[4]);
		T::mulLoadShift8(vST[1], a.xBF[4], b.xBF[2]);
		vST[0] += vST[1];

		// c_0_5 = a1b4（初代入）
		T::mulLoadShift8(c.xF6[0].xBF[5], a.xBF[1], b.xBF[4]);
		T::mulLoadShift8(vST[1], a.xBF[4], b.xBF[1]);
		// c_0_5 = a1b4 + a4b1
		c.xF6[0].xBF[5] += vST[1];
		// (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
		c.xF6[0].xBF[5] += vST[0];

		// (fix) c_0_3 += a2b4 + a4b2
		c.xF6[0].xBF[3] += vST[0];

		// (fix) c_0_2 += a2b4 + a4b2
		c.xF6[0].xBF[2] += vST[0];

		// (fix) c_0_1 += a4b4
		c.xF6[0].xBF[1] += v1;

		// c_0_0 += a2b4 + a4b2
		c.xF6[0].xBF[0] += vST[0];
		// (fix) c_0_0 += a4b4
		c.xF6[0].xBF[0] += v1;

		T::mulLoadShift8(vST[1], a.xBF[0], b.xBF[4]);
		// c_0_4 += a0b4
		c.xF6[0].xBF[4] += vST[1];

		T::mulLoadShift8(vST[1], a.xBF[4], b.xBF[0]);
		// (fix) c_0_4 += a4b0
		c.xF6[0].xBF[4] += vST[1];

		// c_0 + w^5 + w^3
		c.xF6[0].xBF[3].xReg[0] ^= One128;
		c.xF6[0].xBF[5].xReg[0] ^= One128;
	}

	template<int wSize>
	static void alpha_beta_withWindow(F2_ExtField6_2& c, const F2_ExtField6<T>& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);

		// f + g
		c.xF6[1] = a + b;
		// (fix) c_1 = f + g + 1
		c.xF6[1].xBF[0].xReg[0] ^= One128;

		// c_0 := (a0,a1,a2)(b0,b1,b2)
		// Karatsuba 3
		T v1, vST[3];

		T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[0], a.xBF[0], b.xBF[0]);
		T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[1], b.xBF[1]);
		T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[4], a.xBF[2], b.xBF[2]);

		// vST[k] = (a[i]+a[j])(b[i]+b[j])
		// c_0_0
		T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
		// c_0_4
		T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

		// c_0_1
		c.xF6[0].xBF[1] = vST[0] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[1] -= v1;

		// c_0_2
		c.xF6[0].xBF[2] = vST[1] - c.xF6[0].xBF[0];
		c.xF6[0].xBF[2] -= c.xF6[0].xBF[4];
		c.xF6[0].xBF[2] += v1;

		// c_0_3
		c.xF6[0].xBF[3] = vST[2] - v1;
		c.xF6[0].xBF[3] -= c.xF6[0].xBF[4];

		//(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
		// (fix) v1 = a4b4
		T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[4], b.xBF[4]);
		// (fix) vST[0] = a2b4 + a4b2
		T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[2], b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[2]);
		vST[0] += vST[1];

		// c_0_5 = a1b4（初代入）
		T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[5], a.xBF[1], b.xBF[4]);
		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[1]);
		// c_0_5 = a1b4 + a4b1
		c.xF6[0].xBF[5] += vST[1];
		// (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
		c.xF6[0].xBF[5] += vST[0];

		// (fix) c_0_3 += a2b4 + a4b2
		c.xF6[0].xBF[3] += vST[0];

		// (fix) c_0_2 += a2b4 + a4b2
		c.xF6[0].xBF[2] += vST[0];

		// (fix) c_0_1 += a4b4
		c.xF6[0].xBF[1] += v1;

		// c_0_0 += a2b4 + a4b2
		c.xF6[0].xBF[0] += vST[0];
		// (fix) c_0_0 += a4b4
		c.xF6[0].xBF[0] += v1;

		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0], b.xBF[4]);
		// c_0_4 += a0b4
		c.xF6[0].xBF[4] += vST[1];

		T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[0]);
		// (fix) c_0_4 += a4b0
		c.xF6[0].xBF[4] += vST[1];

		// c_0 + w^5 + w^3
		c.xF6[0].xBF[3].xReg[0] ^= One128;
		c.xF6[0].xBF[5].xReg[0] ^= One128;
	}

	/*

	final addition / doublings におけるoperandの一方がsparseな元の乗算
	c = aB; a: normal, B: sparse
	B = (b,1) = s + b
	aB = (a0,a1)(b,1) = (a0 + a1(b+1))s + a0b + a1(w^5 + w^3)

	!! 乗算はregSizeのComb methodで行う !!

	*/
	static void sparseMul(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField6<T> tmp;

		// c_0 = b + 1
		c.xF6[0] = b;
		c.xF6[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField6<T>::mulCombR2L_RegSize(c.xF6[1], a.xF6[1], c.xF6[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF6[1] += a.xF6[0];



		// tmp = a1(w^5 + w^3)
		F2_ExtField6<T>::mulConstTerm(tmp, a.xF6[1]);
		// c_0 = a0b
		F2_ExtField6<T>::mulCombR2L_RegSize(c.xF6[0], a.xF6[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF6[0] += tmp;
	}

	static void sparseMulLoadShift8(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField6<T> tmp;

		// c_0 = b + 1
		c.xF6[0] = b;
		c.xF6[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField6<T>::mulLoadShift8(c.xF6[1], a.xF6[1], c.xF6[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF6[1] += a.xF6[0];



		// tmp = a1(w^5 + w^3)
		F2_ExtField6<T>::mulConstTerm(tmp, a.xF6[1]);
		// c_0 = a0b
		F2_ExtField6<T>::mulLoadShift8(c.xF6[0], a.xF6[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF6[0] += tmp;
	}

	template<int wSize>
	static void sparseMul_withWindow(F2_ExtField6_2& c, const F2_ExtField6_2& a, const F2_ExtField6<T>& b) {

		const char2_128 One128(1,0,0,0);
		F2_ExtField6<T> tmp;

		// c_0 = b + 1
		c.xF6[0] = b;
		c.xF6[0].xBF[0].xReg[0] ^= One128;
		// c_1 = a1(b + 1)
		F2_ExtField6<T>::template mulCombL2RwithWindow<wSize>(c.xF6[1], a.xF6[1], c.xF6[0]);
		// (fix) c_1 = a1(b + 1) + a0
		c.xF6[1] += a.xF6[0];



		// tmp = a1(w^5 + w^3)
		F2_ExtField6<T>::mulConstTerm(tmp, a.xF6[1]);
		// c_0 = a0b
		F2_ExtField6<T>::template mulCombL2RwithWindow<wSize>(c.xF6[0], a.xF6[0], b);
		// (fix) c_0 a0b + a1(w^5 + w^3)
		c.xF6[0] += tmp;
	}
};
