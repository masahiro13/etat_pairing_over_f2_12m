//#define BOOST

#include <iostream>
#include <stdint.h>
#include <time.h>

#ifdef BOOST
#include <boost/timer/timer.hpp>
#endif

#include "F2_511.h"
#include "extension_field6_2.h"
#include "extension_field3_2_2.h"
#include "pairing.h"

void timingF12_6_2(){




	F2_511_r128 A, B, C;
	A.setRandom();
	B.setRandom();

	F2_ExtField6<F2_511_r128> A6, B6, alpha, beta, C6;
	A6.setRandom();
	B6.setRandom();
	alpha.setRandom();
	beta.setRandom();
	alpha.xBF[3].clear();
	alpha.xBF[5].clear();
	beta.xBF[3].clear();
	beta.xBF[5].clear();

	F2_ExtField6_2<F2_511_r128> A12, B12, C12;
	A12.setRandom();
	B12.setRandom();



	clock_t begin, end;
#if 0
	/****************************************************/

	std::cout << "add (F2_511)\n";

	begin = clock();

	C = A + B;

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulShAdd (F2_511)\n";

	begin = clock();

	F2_511_r128::mulShAdd(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombRegSize (F2_511)\n";

	begin = clock();

	F2_511_r128::mulCombR2L_RegSize(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	std::cout << "mulCombL2RWindow3 (F2_511)\n";

	begin = clock();

	F2_511_r128::mulCombL2RwithWindow<3>(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	std::cout << "mulCombL2RWindow4 (F2_511)\n";

	begin = clock();

	F2_511_r128::mulCombL2RwithWindow<4>(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	std::cout << "mulCombL2RWindow5 (F2_511)\n";

	begin = clock();

	F2_511_r128::mulCombL2RwithWindow<5>(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	std::cout << "mulComb<64> (F2_511)\n";

	begin = clock();

	F2_511_r128::mulCombR2L<64>(C, A, B);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	std::cout << "square (F2_511)\n";

	begin = clock();

	F2_511_r128::square(C, A);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;




	/****************************************************/

	std::cout << "add (F12_6)\n";

	begin = clock();

	C6 = A6 + B6;

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulShAdd (F12_6)\n";

	begin = clock();

	F2_ExtField6<F2_511_r128>::mulShAdd(C6, A6, B6);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombRegSize (F12_6)\n";

	begin = clock();

	F2_ExtField6<F2_511_r128>::mulCombR2L_RegSize(C6, A6, B6);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulComb<64> (F12_6)\n";

	begin = clock();

	F2_ExtField6<F2_511_r128>::mulCombR2L<64>(C6, A6, B6);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "square (F12_6)\n";

	begin = clock();

	F2_ExtField6<F2_511_r128>::square(C6, A6);

	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	/****************************************************/

	std::cout << "add (F12_6_2)\n";

	begin = clock();

	C12 = A12 + B12;

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulShAdd (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulShAdd(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombRgegSize (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombR2L_RegSize(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombL2RWindow3 (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombL2RwithWindow<3>(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombL2RWindow4 (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombL2RwithWindow<4>(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombL2RWindow5 (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombL2RwithWindow<5>(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulCombL2RWindow6 (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombL2RwithWindow<6>(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "mulComb<64> (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::mulCombR2L<64>(C12, A12, B12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "square (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::square(C12, A12);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "\\alpha\\beta (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::alpha_beta(C12, alpha, beta);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;



	std::cout << "sparseMul (F12_6_2)\n";

	begin = clock();

	F2_ExtField6_2<F2_511_r128>::sparseMul(C12, A12, B6);

 	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC << " (sec)" << std::endl;


#endif



#if 1
	/*******************************************************************************/
	const int wSize = 6;
	double sum = 0.0;
	const int count = 100000;

	std::cout << "mulCombRgegSize (F2_511)" << std::endl;

	for (int i = 0; i < count; ++i) {
	A.setRandom();
	B.setRandom();

		begin = clock();

		F2_511_r128::mulCombR2L_RegSize(C, A, B);

		end = clock();
		sum += end - begin;
	}

	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;



	std::cout << "mulCombL2RWindow (F2_511) window size: " << wSize << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
	A.setRandom();
	B.setRandom();

		begin = clock();

		F2_511_r128::mulCombL2RwithWindow<wSize>(C, A, B);

		end = clock();
		sum += end - begin;
	}

	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;



	F2_487_r128 A_487, B_487, C_487;
	std::cout << "mulCombRgegSize (F2_487)" << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
	A_487.setRandom();
	B_487.setRandom();

		begin = clock();

		F2_487_r128::mulCombR2L_RegSize(C_487, A_487, B_487);

		end = clock();
		sum += end - begin;
	}

	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;



	std::cout << "mulCombL2RWindow (F2_487) window size: " << wSize << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
	A_487.setRandom();
	B_487.setRandom();

		begin = clock();

		F2_487_r128::mulCombL2RwithWindow<wSize>(C_487, A_487, B_487);

		end = clock();
		sum += end - begin;
	}

	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;




#if 0
	std::cout << "mulCombRegSize (F12_6_2, m=511)" << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
		A12.setRandom();
		B12.setRandom();

		begin = clock();

		F2_ExtField6_2<F2_511_r128>::mulCombR2L_RegSize(C12, A12, B12);

		end = clock();
		sum += end - begin;
	}
	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;



	std::cout << "mulCombL2RWindow (F12_6_2, m=511) window size: " << wSize << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
		A12.setRandom();
		B12.setRandom();

		begin = clock();

		F2_ExtField6_2<F2_511_r128>::mulCombL2RwithWindow<wSize>(C12, A12, B12);

		end = clock();
		sum += end - begin;
	}
	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;



	std::cout << "square (F12_6_2, m=511)" << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
		A12.setRandom();

		begin = clock();

		F2_ExtField6_2<F2_511_r128>::square(C12, A12);

		end = clock();
		sum += end - begin;
	}
	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;
#endif

#if 0
	std::cout << "\\alpha\\beta (F12_6_2, m=511)" << std::endl;

	sum = 0.0;
	for (int i = 0; i < count; ++i) {
		begin = clock();

		F2_ExtField6_2<F2_511_r128>::alpha_beta(C12, alpha, beta);

		end = clock();
		sum += end - begin;
	}
	std::cout << sum / (double) CLOCKS_PER_SEC / count << " (sec), "  << count << "ρs" << std::endl;
#endif

#endif


// boostp
#ifdef BOOST

	boost::timer::cpu_timer timer;
	std::string resultTime;



	/****************************************************/

	std::cout << "add (F2_511)\n";

	timer.start();

	C = A + B;

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulShAdd (F2_511)\n";

	timer.resume();
	timer.start();

	F2_511_r128::mulShAdd(C, A, B);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulCombRegSize (F2_511)\n";

	timer.resume();
	timer.start();

	F2_511_r128::mulCombR2L_RegSize(C, A, B);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulComb<64> (F2_511)\n";

	timer.resume();
	timer.start();

	F2_511_r128::mulCombR2L<64>(C, A, B);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "square (F2_511)\n";

	timer.resume();
	timer.start();

	F2_511_r128::square(C, A);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	/****************************************************/

	std::cout << "add (F12_6)\n";

	timer.resume();
	timer.start();

	C6 = A6 + B6;

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulShAdd (F12_6)\n";

	timer.resume();
	timer.start();

	F2_ExtField6<F2_511_r128>::mulShAdd(C6, A6, B6);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulCombRegSize (F12_6)\n";

	timer.resume();
	timer.start();

	F2_ExtField6<F2_511_r128>::mulCombR2L_RegSize(C6, A6, B6);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulComb<64> (F12_6)\n";

	timer.resume();
	timer.start();

	F2_ExtField6<F2_511_r128>::mulCombR2L<64>(C6, A6, B6);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "square (F12_6)\n";

	timer.resume();
	timer.start();

	F2_ExtField6<F2_511_r128>::square(C6, A6);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	/****************************************************/

	std::cout << "add (F12_6_2)\n";

	timer.resume();
	timer.start();

	C12 = A12 + B12;

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulShAdd (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::mulShAdd(C12, A12, B12);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulCombRgegSize (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::mulCombR2L_RegSize(C12, A12, B12);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "mulComb<64> (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::mulCombR2L<64>(C12, A12, B12);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "square (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::square(C12, A12);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "\\alpha\\beta (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::alpha_beta(C12, alpha, beta);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;



	std::cout << "sparseMul (F12_6_2)\n";

	timer.resume();
	timer.start();

	F2_ExtField6_2<F2_511_r128>::sparseMul(C12, A12, B6);

	timer.stop();
	resultTime = timer.format(9);
	std::cout << resultTime << std::endl;

#endif

}

void timingPairing511() {

	const int Count = 10;
	const int wSize = 5;

	F2_511_r128 xP, xQ, yP, yQ;
	F2_ExtField6_2<F2_511_r128> P12;
	xP.setRandom();
	xQ.setRandom();
	yP.setRandom();
	yQ.setRandom();

#if 0
	boost::timer::cpu_timer timer;
	boost::timer::nanosecond_type wallSum = 0.0;
	boost::timer::nanosecond_type userSum = 0.0;
	boost::timer::nanosecond_type systemSum = 0.0;
	std::string resultTime;
#endif


	/****************************************************/

	clock_t begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*511}} \n";
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2<F2_511_r128>(xP, xQ, yP, yQ);
	}
	clock_t end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;






	F2_ExtField3_2_2<F2_511_r128> Q12;
	xP.setRandom();
	xQ.setRandom();
	yP.setRandom();
	yQ.setRandom();


	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*511}} \n";
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2<F2_511_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;






	/*************************** use window method ***************************/

	begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*511}} using window method, window size: " << wSize << std::endl;
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2<F2_511_r128,wSize>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;




	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*511}} using window method, window size: " << wSize << std::endl;;
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2<F2_511_r128,wSize>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;




	/*************************** use load shift mult. ***************************/

	begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*511}} using shift load multiplication" << std::endl;
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2LoadShift8<F2_511_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;




	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*511}} using shift load multiplication" << std::endl;;
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2LoadShift8<F2_511_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;
}

void timingPairing487() {

	const int Count = 100;
	const int wSize = 4;

	F2_487_r128 xP, xQ, yP, yQ;
	F2_ExtField6_2<F2_487_r128> P12;
	F2_ExtField3_2_2<F2_487_r128> Q12;
	xP.setRandom();
	xQ.setRandom();
	yP.setRandom();
	yQ.setRandom();

#if 0
	boost::timer::cpu_timer timer;
	boost::timer::nanosecond_type wallSum = 0.0;
	boost::timer::nanosecond_type userSum = 0.0;
	boost::timer::nanosecond_type systemSum = 0.0;
	std::string resultTime;
#endif

	clock_t begin, end;



#if 0
	/****************************************************/

	begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*487}} \n";
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2<F2_487_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;




	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*487}} \n";
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2<F2_487_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;

#endif

#if 0

	/*************************** use window method ***************************/

	begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*487}} using window method, window size: " << wSize << std::endl;
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2<F2_487_r128,wSize>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;

#endif


	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*487}} using window method, window size: " << wSize << std::endl;;
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2<F2_487_r128,wSize>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;



#if 0

	/*************************** use load shift multiplication ***************************/

	begin = clock();
	std::cout << "pairing6_2 over F_{2^{12*487}} using shift load multiplication" << std::endl;
	for (int i = 0; i < Count; ++i) {

		P12 = etaT_pairing6_2LoadShift8<F2_487_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)"  << std::endl;




	/****************************************************/

	begin = clock();
	std::cout << "pairing3_2_2 over F_{2^{12*487}} using shift load multiplication" << std::endl;;
	for (int i = 0; i < Count; ++i) {

		Q12 = etaT_pairing3_2_2LoadShift8<F2_487_r128>(xP, xQ, yP, yQ);
	}
	end = clock();
	std::cout << (end - begin) / (double) CLOCKS_PER_SEC / Count << " (sec)" << std::endl;

#endif
}